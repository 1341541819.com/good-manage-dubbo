/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service;

import com.ztzq.check.beans.CheckInRules;
import com.ztzq.check.beans.Reward;
import com.ztzq.check.beans.RewardCondition;
import com.ztzq.check.beans.UserCheckInType;

import java.util.List;

/**
 * 签到规则管理Service
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年04月19日 15:00:06
 */
public interface CheckInRulesService {

	/**
	 * 查询所有签到规则管理
	 * @return
	 */
	public List<CheckInRules> getListCheckInRulesPage(CheckInRules checkinrules);

	/**
	 * 根据条件查询符合的总数
	 * @param checkinrules
	 * @return
	 */
	public Integer countListCheckInRulesPage(CheckInRules checkinrules);

	/**
	 * 根据id查询签到规则管理
	 * @param id
	 * @return
	 */
	public CheckInRules findCheckInRulesById(String id);


	/**
	 * 保存签到规则管理
	 * @param checkinrules
	 */
	public Integer save(CheckInRules checkinrules);


	/**
	 * 根据id更新签到规则管理
	 * @param checkinrules
	 */
	public Integer update(CheckInRules checkinrules);


	/**
	 * 更新删除状态
	 * @param ids
	 * @param parseInt
	 * @return
	 */
	public Integer updateStatus(String id, int parseInt);

	/**
	 * 获取所有奖励条件用于select框显示
	 * @return
	 */
	public List<RewardCondition> getAllRewardCondition();

	/**
	 * 获取所有的奖励用于select框显示
	 * @return
	 */
	public List<Reward> getAllReward();

	/**
	 * 获取所有的签到条件用于select框显示
	 * @return
	 */
	public List<UserCheckInType> getAllUserCheckInType();
	
}
