/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service;

import com.github.pagehelper.PageInfo;
import com.ztzq.check.beans.CheckInOneDayMessage;

/**
 * 日流水信息Service
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年05月05日 11:30:13
 */
public interface CheckInOneDayMessageService {

	/**
	 * 分页查询所有日流水信息
	 * @return
	 */
	public PageInfo<CheckInOneDayMessage> getListCheckInOneDayMessagePage(CheckInOneDayMessage checkinonedaymessage);

	/**
	 * 根据id查询日流水信息
	 * @param id
	 * @return
	 */
	public CheckInOneDayMessage findCheckInOneDayMessageById(String id);


	/**
	 * 保存日流水信息
	 * @param checkinonedaymessage
	 */
	public Integer save(CheckInOneDayMessage checkinonedaymessage);


	/**
	 * 根据id更新日流水信息
	 * @param checkinonedaymessage
	 */
	public Integer update(CheckInOneDayMessage checkinonedaymessage);


	/**
	 * 更新删除状态
	 * @param ids
	 * @param parseInt
	 * @return
	 */
	public Integer updateStatus(String id, int parseInt);
	
}
