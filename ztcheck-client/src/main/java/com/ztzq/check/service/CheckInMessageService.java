/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service;

import com.github.pagehelper.PageInfo;
import com.ztzq.check.beans.CheckInMessage;

import java.util.List;

/**
 * 签到流水信息Service
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年04月19日 10:54:18
 */
public interface CheckInMessageService {

	/**
	 * 查询所有签到流水信息
	 * @return
	 */
	public /*List<CheckInMessage>*/PageInfo<CheckInMessage> getListCheckInMessagePage(CheckInMessage checkInMessage);

	/**
	 * 根据条件查询符合的总数
	 * @param checkInMessage
	 * @return
	 */
	public Integer countListCheckInMessagePage(CheckInMessage checkInMessage);

	/**
	 * 根据id查询签到流水信息
	 * @param id
	 * @return
	 */
	public CheckInMessage findCheckInMessageById(String id);


	/**
	 * 保存签到流水信息
	 * @param checkInMessage
	 */
	public Integer save(CheckInMessage checkInMessage);


	/**
	 * 根据id更新签到流水信息
	 * @param checkInMessage
	 */
	public Integer update(CheckInMessage checkInMessage);


	/**
	 * 更新删除状态
	 * @param ids
	 * @param parseInt
	 * @return
	 */
	public Integer updateStatus(String id, int parseInt);
	
}
