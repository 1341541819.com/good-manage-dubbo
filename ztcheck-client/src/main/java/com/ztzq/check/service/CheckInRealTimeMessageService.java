/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service;

import com.github.pagehelper.PageInfo;
import com.ztzq.check.beans.CheckInRealTimeMessage;

/**
 * 日实时签到流水信息Service
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年05月08日 13:16:02
 */
public interface CheckInRealTimeMessageService {

	/**
	 * 分页查询所有日实时签到流水信息
	 * @return
	 */
	public PageInfo<CheckInRealTimeMessage> getListCheckInRealTimeMessagePage(CheckInRealTimeMessage checkinrealtimemessage);

	/**
	 * 根据id查询日实时签到流水信息
	 * @param id
	 * @return
	 */
	public CheckInRealTimeMessage findCheckInRealTimeMessageById(String id);


	/**
	 * 保存日实时签到流水信息
	 * @param checkinrealtimemessage
	 */
	public Integer save(CheckInRealTimeMessage checkinrealtimemessage);


	/**
	 * 根据id更新日实时签到流水信息
	 * @param checkinrealtimemessage
	 */
	public Integer update(CheckInRealTimeMessage checkinrealtimemessage);


	/**
	 * 更新删除状态
	 * @param ids
	 * @param parseInt
	 * @return
	 */
	public Integer updateStatus(String id, int parseInt);
	
}
