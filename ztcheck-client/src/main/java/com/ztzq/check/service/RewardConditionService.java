/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service;

import com.ztzq.check.beans.RewardCondition;

import java.util.List;

/**
 * 签到奖励条件Service
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年4月17日
 */
public interface RewardConditionService {

	/**
	 * 查询所有奖励条件
	 * @return
	 */
	public List<RewardCondition> getListRewardConditionPage(RewardCondition rewardCondition);

	/**
	 * 根据条件查询符合的总数
	 * @param reward
	 * @return
	 */
	public Integer countListRewardConditionPage(RewardCondition rewardCondition);

	/**
	 * 根据id查询奖品条件
	 * @param id
	 * @return
	 */
	public RewardCondition findRewardConditionById(String id);


	/**
	 * 保存奖品条件
	 * @param reward
	 */
	public Integer save(RewardCondition rewardCondition);


	/**
	 * 根据id更新奖品条件
	 * @param reward
	 */
	public Integer update(RewardCondition rewardCondition);

	/**
	 * 更新删除状态
	 * @param id
	 * @param parseInt
	 * @return
	 */
	public Integer updateStatus(String id, int parseInt);
	
}
