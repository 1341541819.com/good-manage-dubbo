/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service;

import com.ztzq.check.beans.UserCheckInType;

import java.util.List;

/**
 * 签到类型管理Service
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年04月18日 16:04:24
 */
public interface UserCheckInTypeService {

	/**
	 * 查询所有签到类型管理
	 * @return
	 */
	public List<UserCheckInType> getListUserCheckInTypePage(UserCheckInType usercheckintype);

	/**
	 * 根据条件查询符合的总数
	 * @param usercheckintype
	 * @return
	 */
	public Integer countListUserCheckInTypePage(UserCheckInType usercheckintype);

	/**
	 * 根据id查询签到类型管理
	 * @param id
	 * @return
	 */
	public UserCheckInType findUserCheckInTypeById(String id);


	/**
	 * 保存签到类型管理
	 * @param usercheckintype
	 */
	public Integer save(UserCheckInType usercheckintype);


	/**
	 * 根据id更新签到类型管理
	 * @param usercheckintype
	 */
	public Integer update(UserCheckInType usercheckintype);


	/**
	 * 更新删除状态
	 * @param ids
	 * @param parseInt
	 * @return
	 */
	public Integer updateStatus(String id, int parseInt);
	
}
