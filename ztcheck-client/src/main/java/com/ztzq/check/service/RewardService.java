/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service;

import com.github.pagehelper.PageInfo;
import com.ztzq.check.beans.CheckInMessage;
import com.ztzq.check.beans.Reward;

import java.util.List;

/**
 * 签到奖励Service
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年4月17日
 */
public interface RewardService {

	/**
	 * 查询所有奖励
	 * @return
	 */
	public PageInfo<Reward> getListRewardPage(Reward reward);

	/**
	 * 根据条件查询符合的总数
	 * @param reward
	 * @return
	 */
	public Integer countListRewardPage(Reward reward);

	/**
	 * 根据id查询奖品
	 * @param id
	 * @return
	 */
	public Reward findRewardById(String id);


	/**
	 * 保存奖品
	 * @param reward
	 */
	public Integer save(Reward reward);


	/**
	 * 根据id更新奖品
	 * @param reward
	 */
	public Integer update(Reward reward);


	/**
	 * 更新删除状态
	 * @param ids
	 * @param parseInt
	 * @return
	 */
	public Integer updateStatus(String id, int parseInt);
	
}
