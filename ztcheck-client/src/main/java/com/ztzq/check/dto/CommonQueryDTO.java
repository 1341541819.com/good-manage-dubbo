package com.ztzq.check.dto;

/**
 * 公共信息查询封装DTO
 * 
 * @author Jeff Xu
 * @since 2015-12-28
 */
public class CommonQueryDTO {

	private Integer currentPage = 0;//查询起始数

	private Integer pageSize = 10;//查询数量
	
	private Integer totalPage;//总页码数
	
	private String keyword1;//模糊查询关键字1
	
	private String keyword2;//模糊查询关键字2
	
	private String keyword3;//模糊查询关键字3
	
	private String keyword4;//模糊查询关键字4
	
	private String keyword5;//模糊查询关键字5
	
	private String keyword6;//模糊查询关键字6
	
	private String keyword7;//模糊查询关键字7

	
	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public String getKeyword1() {
		return keyword1;
	}

	public void setKeyword1(String keyword1) {
		this.keyword1 = keyword1;
	}

	public String getKeyword2() {
		return keyword2;
	}

	public void setKeyword2(String keyword2) {
		this.keyword2 = keyword2;
	}

	public String getKeyword3() {
		return keyword3;
	}

	public void setKeyword3(String keyword3) {
		this.keyword3 = keyword3;
	}

	public String getKeyword4() {
		return keyword4;
	}

	public void setKeyword4(String keyword4) {
		this.keyword4 = keyword4;
	}

	public String getKeyword5() {
		return keyword5;
	}

	public void setKeyword5(String keyword5) {
		this.keyword5 = keyword5;
	}

	public String getKeyword6() {
		return keyword6;
	}

	public void setKeyword6(String keyword6) {
		this.keyword6 = keyword6;
	}

	public String getKeyword7() {
		return keyword7;
	}

	public void setKeyword7(String keyword7) {
		this.keyword7 = keyword7;
	}
	
}
