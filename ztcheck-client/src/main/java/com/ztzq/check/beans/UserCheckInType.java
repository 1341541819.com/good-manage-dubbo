package com.ztzq.check.beans;

import java.io.Serializable;
import com.ztzq.check.dto.CommonQueryDTO;

/**
 * 
 * 描述:签到类型管理Bean
 * 创建人:章远 
 * 时间:2017年04月18日 16:05:27
 * @version 1.0.0
 * 
 */
public class UserCheckInType extends CommonQueryDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer checkInTypeId;/* 主键 */
	private String checkInDesc;
	private Integer status;// 0已删除1未删除

	public UserCheckInType() {
		super();
	}

	public Integer getCheckInTypeId() {
		return checkInTypeId;
	}

	public void setCheckInTypeId(Integer checkInTypeId) {
		this.checkInTypeId = checkInTypeId;
	}

	public String getCheckInDesc() {
		return checkInDesc;
	}

	public void setCheckInDesc(String checkInDesc) {
		this.checkInDesc = checkInDesc;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
