package com.ztzq.check.beans;

import java.io.Serializable;

import com.ztzq.check.dto.CommonQueryDTO;

@SuppressWarnings("serial")
public class RewardCondition extends CommonQueryDTO implements Serializable{
	//奖励条件id
	private Integer rewardConditionId;
	//奖励条件描述
	private String rewardConditionDesc;
	//奖励条件状态
	private Integer status;
	
	
	public Integer getRewardConditionId() {
		return rewardConditionId;
	}
	public void setRewardConditionId(Integer rewardConditionId) {
		this.rewardConditionId = rewardConditionId;
	}
	public String getRewardConditionDesc() {
		return rewardConditionDesc;
	}
	public void setRewardConditionDesc(String rewardConditionDesc) {
		this.rewardConditionDesc = rewardConditionDesc;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
