package com.ztzq.check.beans;

import java.io.Serializable;
import java.util.Date;
import com.ztzq.check.dto.CommonQueryDTO;

/**
 * 签到流水信息实体类
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年5月3日
 */
@SuppressWarnings("serial")
public class CheckInRealTimeMessage extends CommonQueryDTO implements Serializable{

	//id
	private Integer statisticSlotId;
	//开始时间
	private Date startTime;
	//结束时间
	private Date  endTime;
	//指定时间段签到总人数
	private Integer checkInCount;
	//指定时间段新增签到人数
	private Integer newCount;
	
	public Integer getStatisticSlotId() {
		return statisticSlotId;
	}
	public void setStatisticSlotId(Integer statisticSlotId) {
		this.statisticSlotId = statisticSlotId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getCheckInCount() {
		return checkInCount;
	}
	public void setCheckInCount(Integer checkInCount) {
		this.checkInCount = checkInCount;
	}
	public Integer getNewCount() {
		return newCount;
	}
	public void setNewCount(Integer newCount) {
		this.newCount = newCount;
	}
	
}
