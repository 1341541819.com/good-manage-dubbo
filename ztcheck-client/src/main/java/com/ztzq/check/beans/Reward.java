package com.ztzq.check.beans;

import java.io.Serializable;

import com.ztzq.check.dto.CommonQueryDTO;

/**
 * 奖品bean
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年4月19日
 */
@SuppressWarnings("serial")
public class Reward extends CommonQueryDTO implements Serializable{
	//奖励id
	private Integer rewardId;
	//奖励描述
	private String rewardDesc;
	//奖励状态
	private Integer status;
	
	
	public Integer getRewardId() {
		return rewardId;
	}
	public void setRewardId(Integer rewardId) {
		this.rewardId = rewardId;
	}
	public String getRewardDesc() {
		return rewardDesc;
	}
	public void setRewardDesc(String rewardDesc) {
		this.rewardDesc = rewardDesc;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
