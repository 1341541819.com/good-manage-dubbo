package com.ztzq.check.beans;

import java.io.Serializable;
import java.util.Date;

import com.ztzq.check.dto.CommonQueryDTO;

/**
 * 签到流水信息实体类
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年5月3日
 */
@SuppressWarnings("serial")
public class CheckInMessage extends CommonQueryDTO implements Serializable{

	//id
	private Integer statisticFlowId;
	//用户id
	private String UserId;
	//手机号
	private String  mobile;
	//签到时间
	private Date checkInTime;
	//签到奖励描述
	private String checkInRewardDesc;
	//流水号
	private String checkInflowId;
	//累计签到次数
	private Integer accuCheckIn;
	//连续签到次数
	private Integer contCheckIn;
	//累计奖励总数
	private Integer rewardCount;
	
	public Integer getStatisticFlowId() {
		return statisticFlowId;
	}
	public void setStatisticFlowId(Integer statisticFlowId) {
		this.statisticFlowId = statisticFlowId;
	}
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Date getCheckInTime() {
		return checkInTime;
	}
	public void setCheckInTime(Date checkInTime) {
		this.checkInTime = checkInTime;
	}
	public String getCheckInRewardDesc() {
		return checkInRewardDesc;
	}
	public void setCheckInRewardDesc(String checkInRewardDesc) {
		this.checkInRewardDesc = checkInRewardDesc;
	}
	public String getCheckInflowId() {
		return checkInflowId;
	}
	public void setCheckInflowId(String checkInflowId) {
		this.checkInflowId = checkInflowId;
	}
	public Integer getAccuCheckIn() {
		return accuCheckIn;
	}
	public void setAccuCheckIn(Integer accuCheckIn) {
		this.accuCheckIn = accuCheckIn;
	}
	public Integer getContCheckIn() {
		return contCheckIn;
	}
	public void setContCheckIn(Integer contCheckIn) {
		this.contCheckIn = contCheckIn;
	}
	public Integer getRewardCount() {
		return rewardCount;
	}
	public void setRewardCount(Integer rewardCount) {
		this.rewardCount = rewardCount;
	}
	
}
