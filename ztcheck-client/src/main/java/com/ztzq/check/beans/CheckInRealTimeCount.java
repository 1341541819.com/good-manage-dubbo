package com.ztzq.check.beans;

import java.io.Serializable;
import java.util.Date;
import com.ztzq.check.dto.CommonQueryDTO;
/**
 * 日实时用户签到累计连续信息bean
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年05月19日 18:50:40
 */
@SuppressWarnings("serial")
public class CheckInRealTimeCount extends CommonQueryDTO implements Serializable{
	//用户id
	private String UserId;
	//累计签到次数
	private Integer accuCheckIn;
	//连续签到次数
	private Integer contCheckIn;
	//累计奖励总数
	private Integer rewardCount;
	//最后签到时间
	private Date lastCheckIn;
	
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public Integer getAccuCheckIn() {
		return accuCheckIn;
	}
	public void setAccuCheckIn(Integer accuCheckIn) {
		this.accuCheckIn = accuCheckIn;
	}
	public Integer getContCheckIn() {
		return contCheckIn;
	}
	public void setContCheckIn(Integer contCheckIn) {
		this.contCheckIn = contCheckIn;
	}
	public Integer getRewardCount() {
		return rewardCount;
	}
	public void setRewardCount(Integer rewardCount) {
		this.rewardCount = rewardCount;
	}
	public Date getLastCheckIn() {
		return lastCheckIn;
	}
	public void setLastCheckIn(Date lastCheckIn) {
		this.lastCheckIn = lastCheckIn;
	}
	
}
