package com.ztzq.check.beans;

import java.util.Date;

/**
 * 签到流水信息数据转移实体类
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年5月3日
 */
public class CheckInMessageTransfer {
	//用户id
	private String checkInUserId;
	//手机号
	private String  mobile;
	//签到时间
	private Date checkInDate;
	//流水号
	private String flowId;
	//当日签到奖励数量
	private String rewardParam;
	//奖励描述
	private String rewardUnit;
	//累计签到次数
	private Integer accuCheckIn;
	//连续签到次数
	private Integer contCheckIn;
	//累计奖励总数
	private Integer rewardCount;
	
	public String getCheckInUserId() {
		return checkInUserId;
	}
	public void setCheckInUserId(String checkInUserId) {
		this.checkInUserId = checkInUserId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getRewardParam() {
		return rewardParam;
	}
	public void setRewardParam(String rewardParam) {
		this.rewardParam = rewardParam;
	}
	public String getRewardUnit() {
		return rewardUnit;
	}
	public void setRewardUnit(String rewardUnit) {
		this.rewardUnit = rewardUnit;
	}
	public Integer getAccuCheckIn() {
		return accuCheckIn;
	}
	public void setAccuCheckIn(Integer accuCheckIn) {
		this.accuCheckIn = accuCheckIn;
	}
	public Integer getContCheckIn() {
		return contCheckIn;
	}
	public void setContCheckIn(Integer contCheckIn) {
		this.contCheckIn = contCheckIn;
	}
	public Integer getRewardCount() {
		return rewardCount;
	}
	public void setRewardCount(Integer rewardCount) {
		this.rewardCount = rewardCount;
	}
}
