package com.ztzq.check.beans;

import java.io.Serializable;
import java.util.Date;
import com.ztzq.check.dto.CommonQueryDTO;

/**
 * 日流水信息bean
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年05月05日 11:30:13
 */
@SuppressWarnings("serial")
public class CheckInOneDayMessage extends CommonQueryDTO implements Serializable{
	
	//id
	private Integer statistiDailyId;
	//签到时间
	private Date checkInDate;
	//签到总人数
	private Integer totalCount;
	//新增签到人数
	private Integer newCount;
	//连续签到人数
	private Integer contCount;
	//截止当前时间前一天累计签到人数
	private Integer accuCount;
	//发放积分总数
	private Integer rewardCount;
	
	public Integer getStatistiDailyId() {
		return statistiDailyId;
	}
	public void setStatistiDailyId(Integer statistiDailyId) {
		this.statistiDailyId = statistiDailyId;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Integer getNewCount() {
		return newCount;
	}
	public void setNewCount(Integer newCount) {
		this.newCount = newCount;
	}
	public Integer getContCount() {
		return contCount;
	}
	public void setContCount(Integer contCount) {
		this.contCount = contCount;
	}
	public Integer getAccuCount() {
		return accuCount;
	}
	public void setAccuCount(Integer accuCount) {
		this.accuCount = accuCount;
	}
	public Integer getRewardCount() {
		return rewardCount;
	}
	public void setRewardCount(Integer rewardCount) {
		this.rewardCount = rewardCount;
	}
	
}
