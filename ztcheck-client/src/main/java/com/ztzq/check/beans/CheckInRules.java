package com.ztzq.check.beans;

import java.io.Serializable;
import java.util.Date;
import com.ztzq.check.dto.CommonQueryDTO;

/**
 * 签到规则管理bean
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年04月19日 15:00:06
 */
@SuppressWarnings("serial")
public class CheckInRules extends CommonQueryDTO implements Serializable{
	//签到规则管理id
	private Integer checkInRuleId;
	//奖励条件id
	private Integer rewardCondition;
	//奖励条件
	private String rewardConditionDesc;
	//签到类型id
	private Integer checkInType;
	//签到类型
	private String checkInDesc;
	//签到连续天数
	private Integer checkInDays;
	//签到特殊日期  
	private Date checkInDate;
	//奖励说明id
	private Integer rewardId;
	//奖励说明
	private String rewardDesc;
	//奖励数量
	private Integer rewardAmount;
	//奖励的描述
	private String rewardRemark;
	//奖励的使用状态,1可用,0不可用
	private Integer rewardStatus;
	//奖励规则的优先级，低优先级的先被触发
	private Integer rewardPriori;
	//奖励的单位，天、积分、月等
	private String rewardUnit;
	//奖励规则创建时间
	private Date updateTime;
	
	public Integer getCheckInRuleId() {
		return checkInRuleId;
	}
	public void setCheckInRuleId(Integer checkInRuleId) {
		this.checkInRuleId = checkInRuleId;
	}
	public Integer getRewardCondition() {
		return rewardCondition;
	}
	public void setRewardCondition(Integer rewardCondition) {
		this.rewardCondition = rewardCondition;
	}
	public String getRewardConditionDesc() {
		return rewardConditionDesc;
	}
	public void setRewardConditionDesc(String rewardConditionDesc) {
		this.rewardConditionDesc = rewardConditionDesc;
	}
	public Integer getCheckInType() {
		return checkInType;
	}
	public void setCheckInType(Integer checkInType) {
		this.checkInType = checkInType;
	}
	public String getCheckInDesc() {
		return checkInDesc;
	}
	public void setCheckInDesc(String checkInDesc) {
		this.checkInDesc = checkInDesc;
	}
	public Integer getCheckInDays() {
		return checkInDays;
	}
	public void setCheckInDays(Integer checkInDays) {
		this.checkInDays = checkInDays;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
//	public String getCheckInDate() {
//		return checkInDate;
//	}
//	public void setCheckInDate(String checkInDate) {
//		this.checkInDate = checkInDate;
//	}
	public Integer getRewardId() {
		return rewardId;
	}
	public void setRewardId(Integer rewardId) {
		this.rewardId = rewardId;
	}
	public String getRewardDesc() {
		return rewardDesc;
	}
	public void setRewardDesc(String rewardDesc) {
		this.rewardDesc = rewardDesc;
	}
	public Integer getRewardAmount() {
		return rewardAmount;
	}
	public void setRewardAmount(Integer rewardAmount) {
		this.rewardAmount = rewardAmount;
	}
	public String getRewardRemark() {
		return rewardRemark;
	}
	public void setRewardRemark(String rewardRemark) {
		this.rewardRemark = rewardRemark;
	}
	public Integer getRewardStatus() {
		return rewardStatus;
	}
	public void setRewardStatus(Integer rewardStatus) {
		this.rewardStatus = rewardStatus;
	}
	public Integer getRewardPriori() {
		return rewardPriori;
	}
	public void setRewardPriori(Integer rewardPriori) {
		this.rewardPriori = rewardPriori;
	}
	public String getRewardUnit() {
		return rewardUnit;
	}
	public void setRewardUnit(String rewardUnit) {
		this.rewardUnit = rewardUnit;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}
