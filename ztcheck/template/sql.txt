<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="[daoPackage].[beanName]Dao">
	<!-- 查询所有符合条件的[detail] -->
	<select id="getList[beanName]Page" parameterType="[beanPackage].[beanName]" resultType="[beanPackage].[beanName]">
		SELECT
			t1.check_in_rule_id,
			t1.reward_condition,
			t3.reward_condition_desc,
			t1.check_in_type,
			t4.check_in_desc,
			t1.check_in_days,
			t1.check_in_date,
			t1.reward_id,
			t2.reward_desc,
			t1.reward_amount,
			t1.reward_remark,
			t1.reward_status,
			t1.reward_priori,
			t1.reward_unit,
			t1.update_time
		FROM
			user_[smallBeanName] t1
		LEFT JOIN reward t2 ON t1.reward_id = t2.reward_id
		LEFT JOIN reward_condition t3 ON t1.reward_condition = t3.reward_condition_id
		LEFT JOIN user_check_in_type t4 ON t1.check_in_type = t4.check_in_type_id
		WHERE
			1=1
			<if test="keyword1 != null">	
			AND
				t1.reward_id in (SELECT reward_id FROM reward WHERE reward_desc LIKE '%${keyword1}%')
			</if>
			<if test="keyword2 != null">	
			AND
				t1.reward_condition in (SELECT reward_condition_id FROM reward_condition WHERE reward_condition_desc LIKE '%${keyword2}%')
			</if>
			<if test="keyword3 != null">	
			AND
				t1.check_in_type in (SELECT check_in_type_id FROM user_check_in_type WHERE check_in_desc LIKE '%${keyword3}%')
			</if>
			<if test="keyword4 != null">	
			AND
				t1.check_in_days = '${keyword4}'
			</if>
			<if test="keyword5 != null">	
			AND
				t1.check_in_date = DATE_FORMAT('${keyword5}', '%Y-%m-%d')
			</if>
	</select>
	<!-- 获取所有的[detail]用于select框显示  -->
	<select id="getAllReward" parameterType="[beanPackage].Reward" resultType="[beanPackage].Reward">
		SELECT
			*
		FROM
			reward
		WHERE status = 1
	</select>
	<!-- 获取所有[detail]条件用于select框显示 -->
	<select id="getAllRewardCondition" parameterType="[beanPackage].RewardCondition" resultType="[beanPackage].RewardCondition">
		SELECT
			*
		FROM
			reward_condition
		WHERE status = 1
	</select>
	<!-- 获取所有的[detail]条件用于select框显示 -->
	<select id="getAllUserCheckInType" parameterType="[beanPackage].UserCheckInType" resultType="[beanPackage].UserCheckInType">
		SELECT
			*
		FROM
			user_check_in_type
		WHERE status = 1
	</select>
	<!-- 根据id查询[detail] -->
	<select id="find[beanName]ById"  resultType="[beanPackage].[beanName]">
		SELECT
			t1.check_in_rule_id,
			t1.reward_condition,
			t3.reward_condition_desc,
			t1.check_in_type,
			t4.check_in_desc,
			t1.check_in_days,
			t1.check_in_date,
			t1.reward_id,
			t2.reward_desc,
			t1.reward_amount,
			t1.reward_remark,
			t1.reward_status,
			t1.reward_priori,
			t1.reward_unit,
			t1.update_time
		FROM
			user_[smallBeanName] t1
		LEFT JOIN reward t2 ON t1.reward_id = t2.reward_id
		LEFT JOIN reward_condition t3 ON t1.reward_condition = t3.reward_condition_id
		LEFT JOIN user_check_in_type t4 ON t1.check_in_type = t4.check_in_type_id
		WHERE
			1=1
			<if test="#{id} != null">
			AND
				t1.check_in_rule_id = #{id}
			</if>
	</select>
	<!-- 新增[detail] -->
	<insert id="save" parameterType="[beanPackage].[beanName]">
		INSERT INTO user_[smallBeanName]
		 (
			check_in_rule_id,
			reward_condition,
			check_in_type,
			check_in_days,
			check_in_date,
			reward_id,
			reward_amount,
			reward_remark,
			reward_status,
			reward_priori,
			reward_unit
		)
		VALUES
			(
				NULL,
				#{rewardCondition},
				#{checkInType},
				#{checkInDays},
				#{checkInDate},
				#{rewardId},
				#{rewardAmount},
				#{rewardRemark},
				#{rewardStatus},
				#{rewardPriori},
				#{rewardUnit}
			);
	</insert>
	<!-- 更新[detail]信息 -->
	<update id="update" parameterType="[beanPackage].[beanName]">
		update user_[smallBeanName]
			<set>
				<if test="rewardCondition != null">reward_condition=#{rewardCondition},</if>
				<if test="checkInType != null">check_in_type=#{checkInType},</if>
				<if test="checkInDays != null">check_in_days=#{checkInDays},</if>
				<if test="checkInDate != null">check_in_date=DATE_FORMAT(#{checkInDate}, '%Y-%m-%d'),</if>
				<if test="rewardId != null">reward_id=#{rewardId},</if>
				<if test="rewardAmount != null">reward_amount=#{rewardAmount},</if>
				<if test="rewardRemark != null">reward_remark=#{rewardRemark},</if>
				<if test="rewardStatus != null">reward_status=#{rewardStatus},</if>
				<if test="rewardPriori != null">reward_priori=#{rewardPriori},</if>
				<if test="rewardUnit != null">reward_unit=#{rewardUnit}</if>
			</set>
		where check_in_rule_id = #{checkInRuleId}
	</update>
	<!-- 更新删除信息 -->
	<update id="updateStatus">
		update user_[smallBeanName]
			<set>
				<if test="status != null">reward_status=#{status}</if>
			</set>
		where check_in_rule_id = #{id}
	</update>
	
</mapper>