/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ztzq.check.beans.CheckInRealTimeMessage;
import com.ztzq.check.dao.CheckInRealTimeMessageDao;
import com.ztzq.check.dao.CheckInRealTimeMessageTransferDao;
import com.ztzq.check.service.CheckInRealTimeMessageService;


/**
 * 日实时签到流水信息ServiceImpl
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年05月08日 13:16:02
 */
@Service("CheckInRealTimeMessageService") 
public class CheckInRealTimeMessageServiceImpl implements CheckInRealTimeMessageService {
	private static final Logger logger = Logger.getLogger(CheckInRealTimeMessageServiceImpl.class);
	
	@Autowired
	CheckInRealTimeMessageDao checkinrealtimemessageDao;

	public PageInfo<CheckInRealTimeMessage> getListCheckInRealTimeMessagePage(CheckInRealTimeMessage checkinrealtimemessage) {
		List<CheckInRealTimeMessage> listCheckInRealTimeMessagePage = null;
		PageInfo<CheckInRealTimeMessage> pageInfo =  null;
		try {
			//判断查询时间是否是当天
			if (checkinrealtimemessage.getKeyword1() != null && !checkinrealtimemessage.getKeyword1().equals(getNowYear())) {
				//设置查询时间
				checkinrealtimemessage.setStartTime(new SimpleDateFormat("yyyy-MM-dd 00:00:00").parse(checkinrealtimemessage.getKeyword1()+" 00:00:00"));
				checkinrealtimemessage.setEndTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(checkinrealtimemessage.getKeyword1()+" 23:59:59"));
			}else {
				//设置查询时间
				checkinrealtimemessage.setStartTime(new SimpleDateFormat("yyyy-MM-dd 00:00:00").parse(getHZeroHour()));
				checkinrealtimemessage.setEndTime(new Date());
			}
			PageHelper.startPage(checkinrealtimemessage.getCurrentPage(), checkinrealtimemessage.getPageSize());
			//查询指定时间的所有数据
			listCheckInRealTimeMessagePage = checkinrealtimemessageDao.getListCheckInRealTimeMessagePage(checkinrealtimemessage);
			//判断查询是否次实时查询
			if (checkinrealtimemessage.getKeyword1() == null || checkinrealtimemessage.getKeyword1().equals(getNowYear())) {
				//获取指定时间段签到总人数
				CheckInRealTimeMessage checkInRealTimeMessageCheckInCount = checkinrealtimemessageDao.getAllCheckInRealTimeTransferTotalCount(getNowHour(),getAfterHour());
				//获取指定时间段新增签到总人数
				CheckInRealTimeMessage checkInRealTimeMessage = checkinrealtimemessageDao.getAllCheckInRealTimeTransferNewCount(getNowHour(),getAfterHour());
				checkInRealTimeMessage.setCheckInCount(checkInRealTimeMessageCheckInCount.getCheckInCount());
				if (checkInRealTimeMessage.getCheckInCount() != 0 && checkInRealTimeMessage.getNewCount() != 0) {
					//封装开始时间与结束时间
					checkInRealTimeMessage.setStartTime(new SimpleDateFormat("yyyy-MM-dd HH:00:00").parse(getNowHour()));
					checkInRealTimeMessage.setEndTime(new SimpleDateFormat("yyyy-MM-dd HH:00:00").parse(getAfterHour()));
					//封装实时查询数据
					listCheckInRealTimeMessagePage.add(checkInRealTimeMessage);
				}
			}
			pageInfo = new PageInfo<CheckInRealTimeMessage>(listCheckInRealTimeMessagePage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return pageInfo;
	}

	public CheckInRealTimeMessage findCheckInRealTimeMessageById(String id) {
		CheckInRealTimeMessage checkinrealtimemessage = null;
		try {
			checkinrealtimemessage = checkinrealtimemessageDao.findCheckInRealTimeMessageById(id);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return checkinrealtimemessage;
	}

	public Integer save(CheckInRealTimeMessage checkinrealtimemessage) {
		Integer integer = null;
		try {
			integer = checkinrealtimemessageDao.save(checkinrealtimemessage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer update(CheckInRealTimeMessage checkinrealtimemessage) {
		Integer integer = null;
		try {
			integer = checkinrealtimemessageDao.update(checkinrealtimemessage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer updateStatus(String id, int status) {
		Integer integer = null;
		try {
			integer = checkinrealtimemessageDao.updateStatus(id,status);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	/**
	 * 获取当前时间年月日
	 * @return
	 * @throws ParseException
	 */
	public static String getNowYear() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String date = dateFormat.format(oldDate);
			return date;
	}
	
	/**
	 * 获取当前时间整点
	 * @return
	 * @throws ParseException
	 */
	public static String getNowHour() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
			String date = dateFormat.format(oldDate);
			return date;
	}
	
	/**
	 * 获取当天零点
	 * @return
	 * @throws ParseException
	 */
	public static String getHZeroHour() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
			String date = dateFormat.format(oldDate);
			return date;
	}
	
	/**
	 * 获取当前时间的前一个小时
	 * @return
	 * @throws ParseException
	 */
	public static String getBeforHour() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
		Calendar calendar = new GregorianCalendar(); 
			calendar.setTime(oldDate); 
			calendar.add(calendar.HOUR,-1);//把日期往后增加一天.正数往后推,负数往前移动 
			oldDate = calendar.getTime();//这个时间就是日期往前推一天的结果 
			String date = dateFormat.format(oldDate);
			return date;
	}

	/** 
	 * 获取当前时间的后一个小时
	 * @return
	 * @throws ParseException
	 */
	public static String getAfterHour() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
		Calendar calendar = new GregorianCalendar(); 
			calendar.setTime(oldDate); 
			calendar.add(calendar.HOUR,+1);//把日期往后增加一天.正数往后推,负数往前移动 
			oldDate = calendar.getTime();//这个时间就是日期往前推一天的结果 
			String date = dateFormat.format(oldDate);
			return date;
	}
	
	/*public static void main(String[] args) throws ParseException {
		System.out.println(getNowHour());
	}*/
}
