/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ztzq.check.beans.CheckInOneDayMessage;
import com.ztzq.check.dao.CheckInOneDayMessageDao;
import com.ztzq.check.service.CheckInOneDayMessageService;


/**
 * 日流水信息ServiceImpl
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年05月05日 11:30:13
 */
@Service("CheckInOneDayMessageService") 
public class CheckInOneDayMessageServiceImpl implements CheckInOneDayMessageService {
	private static final Logger logger = Logger.getLogger(CheckInOneDayMessageServiceImpl.class);
	
	@Autowired
	CheckInOneDayMessageDao checkinonedaymessageDao;

	public PageInfo<CheckInOneDayMessage> getListCheckInOneDayMessagePage(CheckInOneDayMessage checkinonedaymessage) {
		List<CheckInOneDayMessage> listCheckInOneDayMessagePage = null;
		PageInfo<CheckInOneDayMessage> pageInfo =  null;
		try {
			PageHelper.startPage(checkinonedaymessage.getCurrentPage(), checkinonedaymessage.getPageSize());
			listCheckInOneDayMessagePage = checkinonedaymessageDao.getListCheckInOneDayMessagePage(checkinonedaymessage);
			pageInfo = new PageInfo<CheckInOneDayMessage>(listCheckInOneDayMessagePage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return pageInfo;
	}

	public CheckInOneDayMessage findCheckInOneDayMessageById(String id) {
		CheckInOneDayMessage checkinonedaymessage = null;
		try {
			checkinonedaymessage = checkinonedaymessageDao.findCheckInOneDayMessageById(id);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return checkinonedaymessage;
	}

	public Integer save(CheckInOneDayMessage checkinonedaymessage) {
		Integer integer = null;
		try {
			integer = checkinonedaymessageDao.save(checkinonedaymessage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer update(CheckInOneDayMessage checkinonedaymessage) {
		Integer integer = null;
		try {
			integer = checkinonedaymessageDao.update(checkinonedaymessage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer updateStatus(String id, int status) {
		Integer integer = null;
		try {
			integer = checkinonedaymessageDao.updateStatus(id,status);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}


}
