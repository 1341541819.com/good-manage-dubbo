/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ztzq.check.beans.CheckInMessage;
import com.ztzq.check.dao.CheckInMessageDao;
import com.ztzq.check.service.CheckInMessageService;


/**
 * 签到流水信息ServiceImpl
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年04月19日 10:54:18
 */
@Service("CheckInMessageService") 
public class CheckInMessageServiceImpl implements CheckInMessageService {
	private static final Logger logger = Logger.getLogger(CheckInMessageServiceImpl.class);
	
	@Autowired
	CheckInMessageDao checkinMessageDao;

	public  /*List<CheckInMessage>*/PageInfo<CheckInMessage> getListCheckInMessagePage(CheckInMessage checkInMessage) {
		List<CheckInMessage> listCheckInMessagePage = null;
		PageInfo<CheckInMessage> pageInfo =  null;
		try {
			PageHelper.startPage(checkInMessage.getCurrentPage(), checkInMessage.getPageSize());
			listCheckInMessagePage = checkinMessageDao.getListCheckInMessagePage(checkInMessage);
			pageInfo = new PageInfo<CheckInMessage>(listCheckInMessagePage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return pageInfo;
	}

	public Integer countListCheckInMessagePage(CheckInMessage checkInMessage) {
		Integer totalcount = null;
		try {
			totalcount = checkinMessageDao.countListCheckInMessagePage(checkInMessage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return totalcount;
	}

	public CheckInMessage findCheckInMessageById(String id) {
		CheckInMessage checkin = null;
		try {
			checkin = checkinMessageDao.findCheckInMessageById(id);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return checkin;
	}

	public Integer save(CheckInMessage checkInMessage) {
		Integer integer = null;
		try {
			integer = checkinMessageDao.save(checkInMessage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer update(CheckInMessage checkInMessage) {
		Integer integer = null;
		try {
			integer = checkinMessageDao.update(checkInMessage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer updateStatus(String id, int status) {
		Integer integer = null;
		try {
			integer = checkinMessageDao.updateStatus(id,status);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}


}
