/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ztzq.check.beans.CheckInOneDayMessage;
import com.ztzq.check.beans.CheckInRealTimeMessage;
import com.ztzq.check.dao.CheckInOneDayMessageTransferDao;
import com.ztzq.check.dao.CheckInRealTimeMessageTransferDao;
import com.ztzq.check.utils.ITaskService;


/**
 * 日流水实时信息转移Service
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年05月05日 11:30:13
 */
@Service("CheckInRealTimeMessageTransferService") 
public class CheckInRealTimeMessageTransferService implements ITaskService{
	private static final Logger logger = Logger.getLogger(CheckInRealTimeMessageTransferService.class);
	
	@Autowired
	CheckInRealTimeMessageTransferDao checkInRealTimeMessageTransferDao;
	CheckInRealTimeMessage checkInRealTimeMessage = null;

	public void doTask() {
		try {
			//获取指定时间段签到总人数
			CheckInRealTimeMessage checkInRealTimeMessageCheckInCount = checkInRealTimeMessageTransferDao.getAllCheckInRealTimeTransferTotalCount(getBeforHour(),getNowHour());
			//获取指定时间段新增签到总人数
			checkInRealTimeMessage = checkInRealTimeMessageTransferDao.getAllCheckInRealTimeTransferNewCount(getBeforHour(),getNowHour());
			checkInRealTimeMessage.setCheckInCount(checkInRealTimeMessageCheckInCount.getCheckInCount());
			//封装开始时间与结束时间
			checkInRealTimeMessage.setStartTime(new SimpleDateFormat("yyyy-MM-dd HH:00:00").parse(getBeforHour()));
			checkInRealTimeMessage.setEndTime(new SimpleDateFormat("yyyy-MM-dd HH:00:00").parse(getNowHour()));
			//保存实时签到信息
			Integer i = checkInRealTimeMessageTransferDao.saveCheckInRealTimeMessage(checkInRealTimeMessage);
			if (i > 0) {
				System.out.println("实时签到流水信息数据新增" + i + "条");
			}	
			System.out.println("+++++++++状态更新一次++++++++++++");
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 获取当前时间整点
	 * @return
	 * @throws ParseException
	 */
	public static String getNowHour() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
			String date = dateFormat.format(oldDate);
			return date;
	}
	
	/**
	 * 获取当前时间的前一个小时
	 * @return
	 * @throws ParseException
	 */
	public static String getBeforHour() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
		Calendar calendar = new GregorianCalendar(); 
			calendar.setTime(oldDate); 
			calendar.add(calendar.HOUR,-1);//把日期往后增加一天.正数往后推,负数往前移动 
			oldDate = calendar.getTime();//这个时间就是日期往前推一天的结果 
			String date = dateFormat.format(oldDate);
			return date;
	}

	/** 
	 * 获取当前时间的后一个小时
	 * @return
	 * @throws ParseException
	 */
	public static String getAfterHour() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
		Calendar calendar = new GregorianCalendar(); 
			calendar.setTime(oldDate); 
			calendar.add(calendar.HOUR,+1);//把日期往后增加一天.正数往后推,负数往前移动 
			oldDate = calendar.getTime();//这个时间就是日期往前推一天的结果 
			String date = dateFormat.format(oldDate);
			return date;
	}
	
	/*public static void main(String[] args) throws ParseException {
		System.out.println(getNowHour());
		System.out.println(getBeforHour());
		System.out.println(getAfterHour());
	}*/
	
}
