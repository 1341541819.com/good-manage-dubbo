package com.ztzq.check.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ztzq.check.beans.CheckInMessage;
import com.ztzq.check.beans.CheckInMessageTransfer;
import com.ztzq.check.dao.CheckInMessageTransferDao;
import com.ztzq.check.utils.ITaskService;

/**
 * 签到流水信息数据转移Service
 * @author 章远
 * @name 
 * ZTCheckClient
 * 2017年5月3日
 */
@Service("CheckInMessageTransferService")
public class CheckInMessageTransferService implements ITaskService{
	private static final Logger logger = Logger.getLogger(CheckInMessageTransferService.class);
	
	@Autowired
	CheckInMessageTransferDao checkInMessageTransferDao;
	Integer sum = 0;
	
	public void doTask() {
		try {
			//获取所有需要转移的数据信息
			List<CheckInMessageTransfer> checkInMessageTransfers = checkInMessageTransferDao.getChekInMessageTransferDataList(getBeforDate());
			
			CheckInMessage checkInMessage = new CheckInMessage();
			for (CheckInMessageTransfer checkInMessageTransfer : checkInMessageTransfers) {
				if (checkInMessageTransfer.getCheckInUserId() != null) {
					checkInMessage.setUserId(checkInMessageTransfer.getCheckInUserId());
				}
				if (checkInMessageTransfer.getMobile() != null) {
					checkInMessage.setMobile(checkInMessageTransfer.getMobile());
				}
				if (checkInMessageTransfer.getCheckInDate() != null) {
					checkInMessage.setCheckInTime(checkInMessageTransfer.getCheckInDate());
				}
				if (checkInMessageTransfer.getFlowId() != null) {
					checkInMessage.setCheckInflowId(checkInMessageTransfer.getFlowId());
				}
				if (checkInMessageTransfer.getRewardParam() != null && checkInMessageTransfer.getRewardUnit() != null) {
					checkInMessage.setCheckInRewardDesc(checkInMessageTransfer.getRewardParam() +" "+ checkInMessageTransfer.getRewardUnit());
				}
				//进行数据转移
				Integer i = checkInMessageTransferDao.saveCheckInMessage(checkInMessage);
				if (i > 0) {
					sum = sum + i;
					System.out.println("签到流水信息数据转移成功" + sum + "条");
				}
			}
			System.out.println("+++++++++状态更新一次++++++++++++");
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取当前时间的前一天
	 * @return
	 * @throws ParseException
	 */
	public String getBeforDate() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = new GregorianCalendar(); 
			calendar.setTime(oldDate); 
			calendar.add(calendar.DATE,-1);//把日期往后增加一天.正数往后推,负数往前移动 
			oldDate = calendar.getTime();//这个时间就是日期往前推一天的结果 
			String date = dateFormat.format(oldDate);
			return date;
	}
}
