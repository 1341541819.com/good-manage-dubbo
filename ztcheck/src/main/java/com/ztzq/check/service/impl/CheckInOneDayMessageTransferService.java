/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ztzq.check.beans.CheckInOneDayMessage;
import com.ztzq.check.dao.CheckInOneDayMessageTransferDao;
import com.ztzq.check.utils.ITaskService;


/**
 * 日流水信息转移Service
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年05月05日 11:30:13
 */
@Service("CheckInOneDayMessageTransferService") 
public class CheckInOneDayMessageTransferService implements ITaskService{
	private static final Logger logger = Logger.getLogger(CheckInOneDayMessageTransferService.class);
	
	@Autowired
	CheckInOneDayMessageTransferDao checkInOneDayMessageTransferDao;

	public void doTask() {
		try {
			//获取当前时间前一天签到流水统计相关信息
			List<CheckInOneDayMessage> checkInOneDayMessages = checkInOneDayMessageTransferDao.getAllCheckInTransferData(getBeforDate());
			if (checkInOneDayMessages != null && checkInOneDayMessages.size() > 0) {
				//将获取数据新增到转存数据表
				Integer i = checkInOneDayMessageTransferDao.saveCheckInOneDayMessage(checkInOneDayMessages.get(0));
				if (i > 0) {
					System.out.println("日签到流水信息数据新增" + i + "条");
				}	
			}else {
				System.out.println("日签到流水信息数据新增失败,未查询到数据");
			}
			//获取当前时间前一天的积分总数
			CheckInOneDayMessage checkInOneDayMessage = checkInOneDayMessageTransferDao.getCountNumReward(getNewYearBeforDate());
			if (checkInOneDayMessage.getRewardCount() != null) {
				//更新获取的积分总数到数据表
				Integer j = checkInOneDayMessageTransferDao.updateCheckInOneDayMessageRewardCount(checkInOneDayMessage.getRewardCount(),getBeforDate());
				if (j > 0) {
					System.out.println("日签到流水信息积分总数数据更新" + j + "条");
				}
			}else {
				System.out.println("日签到流水信息积分总数数据更新失败,未查询到数据");
			}
			//获取当前时间前一天的累计签到人数
			checkInOneDayMessage = checkInOneDayMessageTransferDao.getBeforeDayAccuCount(getBeforDate());
			if (checkInOneDayMessage.getAccuCount() != null) {
				//更新获取的累计签到人数到数据表
				Integer k = checkInOneDayMessageTransferDao.updateCheckInOneDayMessageAccuCount(checkInOneDayMessage.getAccuCount(),getBeforDate());
				if (k > 0) {
					System.out.println("日签到流水信息累计签到总人数数据更新" + k + "条");
				}
			}else {
				System.out.println("日签到流水信息累计签到总人数数据更新失败,未查询到数据");
			}
			System.out.println("+++++++++状态更新一次++++++++++++");
		} catch (ParseException e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		
	}

	/**
	 * 获取当前时间的前一天
	 * @return
	 * @throws ParseException
	 */
	public String getBeforDate() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = new GregorianCalendar(); 
			calendar.setTime(oldDate); 
			calendar.add(calendar.DATE,-1);//把日期往后增加一天.正数往后推,负数往前移动 
			oldDate = calendar.getTime();//这个时间就是日期往前推一天的结果 
			String date = dateFormat.format(oldDate);
			return date;
	}
	
	/**
	 * 获取当前时间的前一天年为yy格式
	 * @return
	 * @throws ParseException
	 */
	public String getNewYearBeforDate() throws ParseException{
		Date oldDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd");
		Calendar calendar = new GregorianCalendar(); 
			calendar.setTime(oldDate); 
			calendar.add(calendar.DATE,-1);//把日期往后增加一天.正数往后推,负数往前移动 
			oldDate = calendar.getTime();//这个时间就是日期往前推一天的结果 
			String date = dateFormat.format(oldDate);
			return date;
	}
	
}
