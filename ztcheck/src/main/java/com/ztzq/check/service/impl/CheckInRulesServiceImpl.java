/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;

import java.util.List;

import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ztzq.check.beans.PageModel;
import com.ztzq.check.beans.Reward;
import com.ztzq.check.beans.RewardCondition;
import com.ztzq.check.beans.UserCheckInType;
import com.ztzq.check.beans.CheckInRules;
import com.ztzq.check.dao.CheckInRulesDao;
import com.ztzq.check.service.CheckInRulesService;


/**
 * 签到规则管理ServiceImpl
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年04月19日 15:00:06
 */
@Service("CheckInRulesService") 
public class CheckInRulesServiceImpl implements CheckInRulesService {
	private static final Logger logger = Logger.getLogger(CheckInRulesServiceImpl.class);
	
	@Autowired
	CheckInRulesDao checkinrulesDao;

	public List<CheckInRules> getListCheckInRulesPage(CheckInRules checkinrules) {
		List<CheckInRules> listCheckInRulesPage = null;
		try {
			listCheckInRulesPage = checkinrulesDao.getListCheckInRulesPage(checkinrules);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return listCheckInRulesPage;
	}

	public Integer countListCheckInRulesPage(CheckInRules checkinrules) {
		Integer totalcount = null;
		try {
			totalcount = checkinrulesDao.countListCheckInRulesPage(checkinrules);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return totalcount;
	}

	public CheckInRules findCheckInRulesById(String id) {
		CheckInRules checkinrules = null;
		try {
			checkinrules = checkinrulesDao.findCheckInRulesById(id);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return checkinrules;
	}

	public Integer save(CheckInRules checkinrules) {
		Integer integer = null;
		try {
			integer = checkinrulesDao.save(checkinrules);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer update(CheckInRules checkinrules) {
		Integer integer = null;
		try {
			integer = checkinrulesDao.update(checkinrules);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer updateStatus(String id, int status) {
		Integer integer = null;
		try {
			integer = checkinrulesDao.updateStatus(id,status);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public List<RewardCondition> getAllRewardCondition() {
		List<RewardCondition> rewardConditionsList = null;
		try {
			rewardConditionsList = checkinrulesDao.getAllRewardCondition();
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return rewardConditionsList;
	}

	public List<Reward> getAllReward() {
		List<Reward> rewardList = null;
		try {
			rewardList = checkinrulesDao.getAllReward();
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return rewardList;
	}

	public List<UserCheckInType> getAllUserCheckInType() {
		List<UserCheckInType> userCheckInTypeList = null;
		try {
			userCheckInTypeList = checkinrulesDao.getAllUserCheckInType();
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return userCheckInTypeList;
	}

}
