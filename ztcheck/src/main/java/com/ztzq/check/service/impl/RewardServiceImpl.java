/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ztzq.check.beans.Reward;
import com.ztzq.check.dao.RewardDao;
import com.ztzq.check.service.RewardService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


/**
 * 签到奖励ServiceImpl
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年4月14日
 */
@Service("RewardService") 
public class RewardServiceImpl implements RewardService {
	private static final Logger logger = Logger.getLogger(RewardServiceImpl.class);
	
	@Autowired
	RewardDao rewardDao;

	public  PageInfo<Reward> getListRewardPage(Reward reward) {
		List<Reward> listRewardPage = null;
		PageInfo<Reward> pageInfo =  null;
		try {
			PageHelper.startPage(reward.getCurrentPage(), reward.getPageSize());
			listRewardPage = rewardDao.getListRewardPage(reward);
			pageInfo = new PageInfo<Reward>(listRewardPage);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return pageInfo;
	}

	public Integer countListRewardPage(Reward reward) {
		Integer totalcount = null;
		try {
			totalcount = rewardDao.countListRewardPage(reward);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return totalcount;
	}

	public Reward findRewardById(String id) {
		Reward reward = null;
		try {
			reward = rewardDao.findRewardById(id);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return reward;
	}

	public Integer save(Reward reward) {
		Integer integer = null;
		try {
			integer = rewardDao.save(reward);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer update(Reward reward) {
		Integer integer = null;
		try {
			integer = rewardDao.update(reward);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer updateStatus(String id, int status) {
		Integer integer = null;
		try {
			integer = rewardDao.updateStatus(id,status);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}


}
