/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ztzq.check.beans.PageModel;
import com.ztzq.check.beans.UserCheckInType;
import com.ztzq.check.dao.UserCheckInTypeDao;
import com.ztzq.check.service.UserCheckInTypeService;


/**
 * 签到类型管理ServiceImpl
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年04月18日 16:04:24
 */
@Service("UserCheckInTypeService") 
public class UserCheckInTypeServiceImpl implements UserCheckInTypeService {
	private static final Logger logger = Logger.getLogger(UserCheckInTypeServiceImpl.class);
	
	@Autowired
	UserCheckInTypeDao usercheckintypeDao;

	public List<UserCheckInType> getListUserCheckInTypePage(UserCheckInType usercheckintype) {
		List<UserCheckInType> listUserCheckInTypePage = null;
		try {
			listUserCheckInTypePage = usercheckintypeDao.getListUserCheckInTypePage(usercheckintype);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return listUserCheckInTypePage;
	}

	public Integer countListUserCheckInTypePage(UserCheckInType usercheckintype) {
		Integer totalcount = null;
		try {
			totalcount = usercheckintypeDao.countListUserCheckInTypePage(usercheckintype);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return totalcount;
	}

	public UserCheckInType findUserCheckInTypeById(String id) {
		UserCheckInType usercheckintype = null;
		try {
			usercheckintype = usercheckintypeDao.findUserCheckInTypeById(id);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return usercheckintype;
	}

	public Integer save(UserCheckInType usercheckintype) {
		Integer integer = null;
		try {
			integer = usercheckintypeDao.save(usercheckintype);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer update(UserCheckInType usercheckintype) {
		Integer integer = null;
		try {
			integer = usercheckintypeDao.update(usercheckintype);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer updateStatus(String id, int status) {
		Integer integer = null;
		try {
			integer = usercheckintypeDao.updateStatus(id,status);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}


}
