/**
 * Copyright @ 2016 linkstec.com.</br>
 * All right reserved.</br>
 */
package com.ztzq.check.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ztzq.check.beans.RewardCondition;
import com.ztzq.check.dao.RewardConditionDao;
import com.ztzq.check.service.RewardConditionService;


/**
 * 签到奖励条件ServiceImpl
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年4月14日
 */
@Service("RewardConditionService") 
public class RewardConditionServiceImpl implements RewardConditionService {
	private static final Logger logger = Logger.getLogger(RewardConditionServiceImpl.class);
	
	@Autowired
	RewardConditionDao rewardConditionDao;

	public List<RewardCondition> getListRewardConditionPage(RewardCondition rewardCondition) {
		List<RewardCondition> listRewardConditionPage = null;
		try {
			listRewardConditionPage = rewardConditionDao.getListRewardConditionPage(rewardCondition);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return listRewardConditionPage;
	}

	public Integer countListRewardConditionPage(RewardCondition rewardCondition) {
		Integer totalcount = null;
		try {
			totalcount = rewardConditionDao.countListRewardConditionPage(rewardCondition);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return totalcount;
	}

	public RewardCondition findRewardConditionById(String id) {
		RewardCondition RewardCondition = null;
		try {
			RewardCondition = rewardConditionDao.findRewardConditionById(id);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return RewardCondition;
	}

	public Integer save(RewardCondition rewardCondition) {
		Integer integer = null;
		try {
			integer = rewardConditionDao.save(rewardCondition);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer update(RewardCondition rewardCondition) {
		Integer integer = null;
		try {
			integer = rewardConditionDao.update(rewardCondition);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}

	public Integer updateStatus(String id, int status) {
		Integer integer = null;
		try {
			integer = rewardConditionDao.updateStatus(id,status);
		} catch (Exception e) {
			logger.error("错误了=====>" + e);
			e.printStackTrace();
		}
		return integer;
	}


}
