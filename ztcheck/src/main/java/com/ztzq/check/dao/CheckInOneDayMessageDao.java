package com.ztzq.check.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ztzq.check.beans.CheckInOneDayMessage;

/**
 * 日流水信息Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年05月05日 11:30:13
 */
public interface CheckInOneDayMessageDao {

	/**
	 * 查询所有日流水信息
	 * @param checkinonedaymessage
	 */
	public List<CheckInOneDayMessage> getListCheckInOneDayMessagePage(CheckInOneDayMessage checkinonedaymessage);

	/**
	 * 根据条件查询符合的总数
	 * @param checkinonedaymessage
	 * @return
	 */
	public Integer countListCheckInOneDayMessagePage(CheckInOneDayMessage checkinonedaymessage);

	/**
	 * 通过id查询日流水信息
	 * @param id
	 * @return
	 */
	public CheckInOneDayMessage findCheckInOneDayMessageById(@Param(value = "id") String id);

	/**
	 * 保存奖品
	 * @param checkinonedaymessage
	 * @return
	 */
	public Integer save(CheckInOneDayMessage checkinonedaymessage);

	/**
	 * 根据id更新奖品
	 * @param checkinonedaymessage
	 * @return
	 */
	public Integer update(CheckInOneDayMessage checkinonedaymessage);

	/**
	 * 更新删除状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(@Param(value = "id") String id,@Param(value = "status") int status);

}
