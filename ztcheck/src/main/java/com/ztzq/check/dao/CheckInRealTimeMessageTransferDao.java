package com.ztzq.check.dao;

import org.apache.ibatis.annotations.Param;

import com.ztzq.check.beans.CheckInRealTimeMessage;

/**
 * 日流水实时信息转移Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年5月8日
 */
public interface CheckInRealTimeMessageTransferDao {

	/**
	 * 获取指定时间段签到总人数
	 * @param beforHour
	 * @param nowHour
	 * @return
	 */
	CheckInRealTimeMessage getAllCheckInRealTimeTransferTotalCount(@Param(value = "startTime") String nowHour,@Param(value = "endTime") String afterHour);

	/**
	 * 获取指定时间段新增签到总人数
	 * @param beforHour
	 * @param nowHour
	 * @return
	 */
	CheckInRealTimeMessage getAllCheckInRealTimeTransferNewCount(@Param(value = "startTime") String nowHour,@Param(value = "endTime") String afterHour);

	/**
	 * 保存实时签到信息
	 * @param checkInRealTimeMessage
	 * @return
	 */
	Integer saveCheckInRealTimeMessage(CheckInRealTimeMessage checkInRealTimeMessage);

}
