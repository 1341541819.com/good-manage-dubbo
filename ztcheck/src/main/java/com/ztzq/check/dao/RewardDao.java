package com.ztzq.check.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ztzq.check.beans.Reward;

public interface RewardDao {

	/**
	 * 查询所有奖励
	 * @param reward
	 */
	public List<Reward> getListRewardPage(Reward reward);

	/**
	 * 根据条件查询符合的总数
	 * @param reward
	 * @return
	 */
	public Integer countListRewardPage(Reward reward);

	/**
	 * 通过id查询奖励
	 * @param id
	 * @return
	 */
	public Reward findRewardById(@Param(value = "id") String id);

	/**
	 * 保存奖品
	 * @param reward
	 * @return
	 */
	public Integer save(Reward reward);

	/**
	 * 根据id更新奖品
	 * @param reward
	 * @return
	 */
	public Integer update(Reward reward);

	/**
	 * 更新删除状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(@Param(value = "id") String id,@Param(value = "status") int status);

}
