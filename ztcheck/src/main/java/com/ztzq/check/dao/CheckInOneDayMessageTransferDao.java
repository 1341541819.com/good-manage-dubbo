package com.ztzq.check.dao;

import java.awt.Image;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ztzq.check.beans.CheckInOneDayMessage;

/**
 * 日流水信息转移Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年05月05日 11:30:13
 */
public interface CheckInOneDayMessageTransferDao {

	/**
	 * 获取当前时间前一天签到流水统计相关信息
	 * @param checkinonedaymessage
	 */
	public List<CheckInOneDayMessage> getAllCheckInTransferData(@Param(value = "oldDate") String oldDate);

	/**
	 * 保存当前时间前一天签到流水统计相关信息
	 * @param checkInOneDayMessage
	 * @return
	 */
	public Integer saveCheckInOneDayMessage(CheckInOneDayMessage checkInOneDayMessage);

	/**
	 * 获取当前时间前一天的积分总数
	 * @param beforDate
	 * @return
	 */
	public CheckInOneDayMessage getCountNumReward(@Param(value = "oldDate") String oldDate);

	/**
	 * 更新获取的积分总数到数据表
	 * @param checkInOneDayMessage
	 * @param beforDate
	 * @return
	 */
	public Integer updateCheckInOneDayMessageRewardCount(@Param(value = "rewardCount") Integer rewardCount, @Param(value = "oldDate") String oldDate);

	/**
	 * 获取当前时间前一天的累计签到人数
	 * @param beforDate
	 * @return
	 */
	public CheckInOneDayMessage getBeforeDayAccuCount(@Param(value = "oldDate") String oldDate);

	/**
	 * 更新获取的累计签到人数到数据表
	 * @param checkInOneDayMessage
	 * @param beforDate
	 * @return
	 */
	public Integer updateCheckInOneDayMessageAccuCount(@Param(value = "accuCount") Integer accuCount, @Param(value = "oldDate") String oldDate);

}
