package com.ztzq.check.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ztzq.check.beans.CheckInRules;
import com.ztzq.check.beans.Reward;
import com.ztzq.check.beans.RewardCondition;
import com.ztzq.check.beans.UserCheckInType;

/**
 * 签到规则管理Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年04月19日 15:00:06
 */
public interface CheckInRulesDao {

	/**
	 * 查询所有签到规则管理
	 * @param checkinrules
	 */
	public List<CheckInRules> getListCheckInRulesPage(CheckInRules checkinrules);

	/**
	 * 根据条件查询符合的总数
	 * @param checkinrules
	 * @return
	 */
	public Integer countListCheckInRulesPage(CheckInRules checkinrules);

	/**
	 * 通过id查询签到规则管理
	 * @param id
	 * @return
	 */
	public CheckInRules findCheckInRulesById(@Param(value = "id") String id);

	/**
	 * 保存奖品
	 * @param checkinrules
	 * @return
	 */
	public Integer save(CheckInRules checkinrules);

	/**
	 * 根据id更新奖品
	 * @param checkinrules
	 * @return
	 */
	public Integer update(CheckInRules checkinrules);

	/**
	 * 更新删除状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(@Param(value = "id") String id,@Param(value = "status") int status);

	/**
	 * 获取所有奖励条件用于select框显示
	 * @return
	 */
	public List<RewardCondition> getAllRewardCondition();

	/**
	 * 获取所有的奖励用于select框显示
	 * @return
	 */
	public List<Reward> getAllReward();

	/**
	 * 获取所有的签到条件用于select框显示
	 * @return
	 */
	public List<UserCheckInType> getAllUserCheckInType();

}
