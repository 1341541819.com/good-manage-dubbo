package com.ztzq.check.dao;

import com.ztzq.check.beans.CheckInMessage;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 签到流水信息Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年04月19日 10:54:18
 */
public interface CheckInMessageDao {

	/**
	 * 查询所有签到流水信息
	 * @param checkin
	 */
	public List<CheckInMessage> getListCheckInMessagePage(CheckInMessage checkInMessage);

	/**
	 * 根据条件查询符合的总数
	 * @param checkin
	 * @return
	 */
	public Integer countListCheckInMessagePage(CheckInMessage checkInMessage);

	/**
	 * 通过id查询签到流水信息
	 * @param id
	 * @return
	 */
	public CheckInMessage findCheckInMessageById(@Param(value = "id") String id);

	/**
	 * 保存签到流水信息
	 * @param checkin
	 * @return
	 */
	public Integer save(CheckInMessage checkInMessage);

	/**
	 * 根据id更新奖品
	 * @param checkin
	 * @return
	 */
	public Integer update(CheckInMessage checkInMessage);

	/**
	 * 更新删除状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(@Param(value = "id") String id,@Param(value = "status") int status);

}
