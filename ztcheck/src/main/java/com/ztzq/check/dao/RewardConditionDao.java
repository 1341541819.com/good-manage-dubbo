package com.ztzq.check.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.ztzq.check.beans.RewardCondition;

/**
 * 签到奖励条件Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年4月18日
 */
public interface RewardConditionDao {

	/**
	 * 查询所有奖励条件
	 * @param reward
	 */
	public List<RewardCondition> getListRewardConditionPage(RewardCondition rewardCondition);

	/**
	 * 根据条件查询符合的总数
	 * @param reward
	 * @return
	 */
	public Integer countListRewardConditionPage(RewardCondition rewardCondition);

	/**
	 * 根据id查询奖品条件
	 * @param id
	 * @return
	 */
	public RewardCondition findRewardConditionById(@Param(value = "id") String id);

	/**
	 * 保存奖品条件
	 * @param reward
	 * @return
	 */
	public Integer save(RewardCondition rewardCondition);

	/**
	 * 根据id更新奖品条件
	 * @param reward
	 * @return
	 */
	public Integer update(RewardCondition rewardCondition);

	/**
	 * 更新删除状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(@Param(value = "id") String id,@Param(value = "status") int status);

}
