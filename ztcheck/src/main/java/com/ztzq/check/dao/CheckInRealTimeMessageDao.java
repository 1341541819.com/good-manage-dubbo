package com.ztzq.check.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ztzq.check.beans.CheckInRealTimeMessage;

/**
 * 日实时签到流水信息Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年05月08日 13:16:02
 */
public interface CheckInRealTimeMessageDao {

	/**
	 * 查询所有日实时签到流水信息
	 * @param checkinrealtimemessage
	 */
	public List<CheckInRealTimeMessage> getListCheckInRealTimeMessagePage(CheckInRealTimeMessage checkinrealtimemessage);

	/**
	 * 根据条件查询符合的总数
	 * @param checkinrealtimemessage
	 * @return
	 */
	public Integer countListCheckInRealTimeMessagePage(CheckInRealTimeMessage checkinrealtimemessage);

	/**
	 * 通过id查询日实时签到流水信息
	 * @param id
	 * @return
	 */
	public CheckInRealTimeMessage findCheckInRealTimeMessageById(@Param(value = "id") String id);

	/**
	 * 保存奖品
	 * @param checkinrealtimemessage
	 * @return
	 */
	public Integer save(CheckInRealTimeMessage checkinrealtimemessage);

	/**
	 * 根据id更新奖品
	 * @param checkinrealtimemessage
	 * @return
	 */
	public Integer update(CheckInRealTimeMessage checkinrealtimemessage);

	/**
	 * 更新删除状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(@Param(value = "id") String id,@Param(value = "status") int status);

	/**
	 * 获取指定时间段签到总人数
	 * @param nowHour
	 * @param afterHour
	 * @return
	 */
	public CheckInRealTimeMessage getAllCheckInRealTimeTransferTotalCount(@Param(value = "startTime") String nowHour,@Param(value = "endTime") String afterHour);

	/**
	 * 获取指定时间段新增签到总人数
	 * @param nowHour
	 * @param afterHour
	 * @return
	 */
	public CheckInRealTimeMessage getAllCheckInRealTimeTransferNewCount(@Param(value = "startTime") String nowHour,@Param(value = "endTime") String afterHour);

}
