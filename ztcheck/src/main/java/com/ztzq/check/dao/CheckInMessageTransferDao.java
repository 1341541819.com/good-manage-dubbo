package com.ztzq.check.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ztzq.check.beans.CheckInMessage;
import com.ztzq.check.beans.CheckInMessageTransfer;


/**
 * 签到流水信息数据转移Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年5月3日
 */
public interface CheckInMessageTransferDao {
	
	/**
	 * 获取所有需要转移的数据信息
	 * @return
	 */
	public List<CheckInMessageTransfer> getChekInMessageTransferDataList(@Param(value = "oldDate") String oldDate);

	/**
	 * 转存数据信息
	 * @param checkInMessage
	 * @return
	 */
	public Integer saveCheckInMessage(CheckInMessage checkInMessage);
	
	
}
