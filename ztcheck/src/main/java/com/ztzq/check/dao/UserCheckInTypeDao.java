package com.ztzq.check.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ztzq.check.beans.UserCheckInType;

/**
 * 签到类型管理Dao
 * @author 章远
 * @name 
 * ZTCheck
 * 2017年04月18日 16:04:24
 */
public interface UserCheckInTypeDao {

	/**
	 * 查询所有签到类型管理
	 * @param usercheckintype
	 */
	public List<UserCheckInType> getListUserCheckInTypePage(UserCheckInType usercheckintype);

	/**
	 * 根据条件查询符合的总数
	 * @param usercheckintype
	 * @return
	 */
	public Integer countListUserCheckInTypePage(UserCheckInType usercheckintype);

	/**
	 * 通过id查询签到类型管理
	 * @param id
	 * @return
	 */
	public UserCheckInType findUserCheckInTypeById(@Param(value = "id") String id);

	/**
	 * 保存奖品
	 * @param usercheckintype
	 * @return
	 */
	public Integer save(UserCheckInType usercheckintype);

	/**
	 * 根据id更新奖品
	 * @param usercheckintype
	 * @return
	 */
	public Integer update(UserCheckInType usercheckintype);

	/**
	 * 更新删除状态
	 * @param id
	 * @param status
	 * @return
	 */
	public Integer updateStatus(@Param(value = "id") String id,@Param(value = "status") int status);

}
