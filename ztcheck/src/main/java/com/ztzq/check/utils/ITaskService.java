package com.ztzq.check.utils;


/**
 * 定时任务 接口
 * @author sh
 *
 */
public interface ITaskService {
	public void doTask();
}
