package com.ztzq.check.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 * @作者 章远
 * @项目名 moonvip_admin01
 * @时间 2016年12月13日 上午9:21:23
 * @描述 自动生成代码工具类
 * @version 1.0.0
 */
public class ZyAutoProject {

    // bean
    // service
    // dao
    // web/bean
    // pages/bean

    // 类名
    private static String beanName = "CheckInRealTimeMessage";
    private static String smallBeanName = beanName.toLowerCase();//将实体类名首字母转成小写
    // 实体类注释
    private static String beanDetail = "日实时签到流水信息";
    private static String author = "章远";
    private static String date = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss").format(new Date());
    
    // 目录结构
    private static String srcPath = "src/";
    private static String beanPath = "com/ztzq/check/beans";
    private static String daoPath = "com/ztzq/check/dao/";
    private static String servicePath = "com/ztzq/check/service/";
    private static String serviceImplPath = "com/ztzq/check/service/impl";
    private static String webPath = "com/ztzq/check/web";
    private static String pagePath = "webapp/WEB-INF/";
    // 实体模板目录
    private static String beanTemplate = "template/bean.txt";
    private static String daoTemplate = "template/dao.txt";
    private static String serviceTemplate = "template/service.txt";
    private static String serviceImplTemplate = "template/serviceImpl.txt";
    private static String controllerTemplate = "template/controller.txt";
    private static String xmlTemplate = "template/sql.txt";
    private static String listpageTemplate = "template/list.txt";
    private static String editpageTemplate = "template/edit.txt";
    private static String webTemplate = "template/template.txt";
    // 包名
    private static String beanPackage = "com.ztzq.check.beans";
    private static String daoPackage = "com.ztzq.check.dao";
    private static String servicePackage = "com.ztzq.check.service";
    private static String ajaxResultPackage = "com.jeff.tianti.common.dto";
    private static String commonQueryDTOPackage = "com.ztzq.check.dto";
    private static String constantsPackage = "com.jeff.tianti.util";
    private static String serviceImplPackage = "com.ztzq.check.service.impl";
    private static String webPackage = "com.jeff.tianti.check.web";
    //jsp名称
    private static String list = smallBeanName + "_list";
    private static String add = smallBeanName + "_add";
    private static String delete = smallBeanName + "_delete";
    private static String edit = smallBeanName + "_edit";
    private static String template = smallBeanName + "_template";
    /**
     * 创建实体bean的方法
     * @throws IOException
     */
    public static void creatBean() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(srcPath + beanPath);
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File beanFile = new File(rootFile, beanName + ".java");
	// 加载模板
	String templte = getRoot(beanTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//执行替换数据方法
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (beanFile.exists()){
	    System.out.println("[自动构建提示:]您创建的["+beanFile+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(beanFile.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("实体类已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(beanFile.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("实体类已经成功创建!!!");   
	}
	
    }

    /**
     * 创建实体dao的方法
     * @throws IOException
     */
    public static void creatDao() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(srcPath + daoPath + smallBeanName);
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File daoFile = new File(rootFile, beanName+"Dao.java");
	// 加载模板
	String templte = getRoot(daoTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//执行替换数据方法
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (daoFile.exists()){
	    System.out.println("[自动构建提示:]您创建的["+daoFile+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(daoFile.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("dao类已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(daoFile.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("dao类已经成功创建!!!");   
	}
	
    }
    
    /**
     * 创建实体类映射xml文件的方法
     * @throws IOException
     */
    public static void creatSqlXml() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(srcPath + daoPath + smallBeanName);
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File daoFileXml = new File(rootFile, beanName+"Mapper"+".xml");
	// 加载模板
	String templte = getRoot(xmlTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//通过传参执行替换数据
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (daoFileXml.exists()){
	    System.out.println("[自动构建提示:]您创建的["+daoFileXml+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(daoFileXml.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("xml映射文件已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(daoFileXml.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("xml映射文件已经成功创建!!!");   
	}
	
    }
    
    /**
     * 创建实体Service的方法
     * @throws IOException
     */
    public static void creatService() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(srcPath + servicePath + smallBeanName);
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File serviceFile = new File(rootFile, beanName+"Service.java");
	// 加载模板
	String templte = getRoot(serviceTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//执行替换数据方法
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (serviceFile.exists()){
	    System.out.println("[自动构建提示:]您创建的["+serviceFile+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(serviceFile.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("service类已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(serviceFile.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("service类已经成功创建!!!");   
	}
	
    }
    
    /**
     * 创建实体ServiceImpl的方法
     * @throws IOException
     */
    public static void creatServiceImpl() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(srcPath + serviceImplPath + smallBeanName + "/impl");
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File serviceImplFile = new File(rootFile, beanName+"ServiceImpl.java");
	// 加载模板
	String templte = getRoot(serviceImplTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//执行替换数据方法
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (serviceImplFile.exists()){
	    System.out.println("[自动构建提示:]您创建的["+serviceImplFile+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(serviceImplFile.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("service实现类已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(serviceImplFile.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("service实现类已经成功创建!!!");   
	}
	
    }
    
    /**
     * 创建实体Controller的方法
     * @throws IOException
     */
    public static void creatController() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(srcPath + webPath);
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File controllerFile = new File(rootFile, beanName+"Controller.java");
	// 加载模板
	String templte = getRoot(controllerTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//执行替换数据方法
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (controllerFile.exists()){
	    System.out.println("[自动构建提示:]您创建的["+controllerFile+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(controllerFile.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("controller类已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(controllerFile.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("controller类已经成功创建!!!");   
	}
	
    }
    
    /**
     * 创建实体list jsp页面的方法
     * @throws IOException
     */
    public static void creatListJsp() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(pagePath + smallBeanName);
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File JspFile = new File(rootFile, list+".jsp");
	// 加载模板
	String templte = getRoot(listpageTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//执行替换数据方法
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (JspFile.exists()){
	    System.out.println("[自动构建提示:]您创建的["+JspFile+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(JspFile.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("jsp页面已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(JspFile.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("jsp页面已经成功创建!!!");   
	}
	
    }
    
    /**
     * 创建实体edit jsp页面的方法
     * @throws IOException
     */
    public static void creatEditJsp() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(pagePath + smallBeanName);
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File JspFile = new File(rootFile, edit+".jsp");
	// 加载模板
	String templte = getRoot(editpageTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//执行替换数据方法
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (JspFile.exists()){
	    System.out.println("[自动构建提示:]您创建的["+JspFile+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(JspFile.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("jsp页面已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(JspFile.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("jsp页面已经成功创建!!!");   
	}
	
    }
    
    /**
     * 创建实体jspTemplate页面的方法
     * @throws IOException
     */
    public static void creatJspTemplate() throws IOException {
	// 组合创建路径
	String rootPath = getRoot(pagePath + smallBeanName);
	File rootFile = new File(rootPath);
	// 文件夹如果未创建就创建
	if (!rootFile.exists()) {
	    rootFile.mkdirs();
	}
	// 要写入的目标文件
	File JspFile = new File(rootFile, template+".jsp");
	// 加载模板
	String templte = getRoot(webTemplate);
	//File beanTemplteFile = new File(templte);
	// 读取模板数据
	String content = FileUtils.readFileToString(new File(templte), "utf-8");
	//执行替换数据方法
	content = replaceTemplte(content);
	//创建控制台输入对象
	Scanner scanner = new Scanner(System.in);
	if (JspFile.exists()){
	    System.out.println("[自动构建提示:]您创建的["+JspFile+"]已经存在,是否覆盖yes/no!");
	    //获取控制台输入的值
            String mark = scanner.nextLine();
            if (mark.equalsIgnoreCase("yes")){
        	// 通过IO写出数据
        	FileUtils.writeStringToFile(JspFile.getAbsoluteFile(), content,"UTF-8");
        	System.out.println("jspTemplate页面已经成功覆盖!!!");
               }else {
        	   System.out.println("已成功取消覆盖!");
        	   return;
               }
	}else {	    
	    // 第一次创建,通过IO写出数据
	    FileUtils.writeStringToFile(JspFile.getAbsoluteFile(), content,"UTF-8");
	    System.out.println("jspTemplate页面已经成功创建!!!");   
	}
	
    }
    
    /**
     * 替换模板信息
     * @param content 需求修改的模板内容
     * @return
     */
    public static String replaceTemplte(String content) {
	if (StringUtils.isNotBlank(content)) {
	    content = content.replaceAll("\\[beanPackage\\]", beanPackage)
		    .replaceAll("\\[daoPackage\\]", daoPackage)
		    .replaceAll("\\[servicePackage\\]", servicePackage)
		    .replaceAll("\\[serviceImplPackage\\]", serviceImplPackage)
		    .replaceAll("\\[ajaxResultPackage\\]", ajaxResultPackage)
		    .replaceAll("\\[commonQueryDTOPackage\\]", commonQueryDTOPackage)
		    .replaceAll("\\[constantsPackage\\]", constantsPackage)
		    .replaceAll("\\[webPackage\\]", webPackage)
		    .replaceAll("\\[daoPackage\\]", daoPackage)
		    .replaceAll("\\[detail\\]", beanDetail)
		    .replaceAll("\\[author\\]", author)
		    .replaceAll("\\[date\\]", date)
		    .replaceAll("\\[beanName\\]", beanName)
	    	    .replaceAll("\\[smallBeanName\\]", smallBeanName);
	    return content;
	} else {
	    return "";
	}
    }

    /**
     * 获取当前工程项目路径
     * @param path 传入的路径
     * @return
     */
    public static String getRoot(String path) {
	return new File(System.getProperty("user.dri"), path).getAbsolutePath();
    }

    public static void main(String[] args) throws IOException {
//	creatBean();
//	creatDao();
//	creatSqlXml();
//	creatService();
//	creatServiceImpl();
//	creatController();
	creatListJsp();
//	creatEditJsp();
	//creatJspTemplate();
	// System.out.println(getRoot(""));
	// java.util.Properties properties = System.getProperties();
	// Enumeration<Object> keys = properties.keys();
	// while (keys.hasMoreElements()) {
	// Object object = (Object) keys.nextElement();
	// System.out.println(object + "==========" + properties.get(object));
	// }

    }
}
