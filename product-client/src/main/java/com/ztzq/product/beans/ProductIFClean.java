package com.ztzq.product.beans;

import java.io.Serializable;

import com.ztzq.product.dto.CommonQueryDTO;


@SuppressWarnings("serial")
public class ProductIFClean extends CommonQueryDTO implements Serializable{
	/**
	 * 产品代码
	 */
	private String productCode;
	/**
	 * 产品代码
	 */
	private String productName;
	/**
	 * 是否黑白名单(1:黑名单，0：白名单)
	 */
	private String status;

	/**
	 * 产品子类
	 */
	private String OTC_CPZL;
	/**
	 * 产品子类名称
	 */
	private String OTC_CPZL_NAME;
	/**
	 * 产品子类是否显示
	 */
	private String IS_Sure;
		
	
	
	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public ProductIFClean() {
		super();
	
	}


	public String getOTC_CPZL_NAME() {
		return OTC_CPZL_NAME;
	}


	public void setOTC_CPZL_NAME(String oTC_CPZL_NAME) {
		OTC_CPZL_NAME = oTC_CPZL_NAME;
	}


	public String getIS_Sure() {
		return IS_Sure;
	}


	public void setIS_Sure(String iS_Sure) {
		IS_Sure = iS_Sure;
	}


	public ProductIFClean(String oTC_CPZL, String oTC_CPZL_NAME, String iS_Sure) {
		super();
		OTC_CPZL = oTC_CPZL;
		OTC_CPZL_NAME = oTC_CPZL_NAME;
		IS_Sure = iS_Sure;
	}


	public String getOTC_CPZL() {
		return OTC_CPZL;
	}

	public void setOTC_CPZL(String oTC_CPZL) {
		OTC_CPZL = oTC_CPZL;
	}

	public ProductIFClean(String productCode, String status) {
		super();
		this.productCode = productCode;
		this.status = status;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
