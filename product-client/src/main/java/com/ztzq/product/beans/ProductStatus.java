package com.ztzq.product.beans;

import java.io.Serializable;

import com.ztzq.product.dto.CommonQueryDTO;


public class ProductStatus extends CommonQueryDTO implements Serializable{
	/**
	 * 产品代码
	 */
	private String productCode;
	/**
	 * 产品名称
	 */
	private String productName;
	/**
	 * 产品状态
	 */
	private String productStatus;

	
	
	
	public ProductStatus() {
		super();
	}

	public ProductStatus(String productCode, String productName, String productStatus) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.productStatus = productStatus;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

}
