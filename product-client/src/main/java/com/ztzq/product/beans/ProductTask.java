package com.ztzq.product.beans;

import java.io.Serializable;

import com.ztzq.product.dto.CommonQueryDTO;


public class ProductTask extends CommonQueryDTO implements Serializable {

	/**
	 * id
	 */
	private String id;
	/**
	 * 定时任务名称
	 */
	private String task_name;
	
	/**
	 * 产品状态
	 */
	private String status;

	
	
	public ProductTask() {
		super();
	}

	public ProductTask(String id,String task_name, String status) {
		super();
		this.id = id;
		this.task_name = task_name;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTask_name() {
		return task_name;
	}

	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
