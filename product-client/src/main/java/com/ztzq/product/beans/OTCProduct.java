package com.ztzq.product.beans;

import java.io.Serializable;

import com.ztzq.product.dto.CommonQueryDTO;


@SuppressWarnings("serial")
public class OTCProduct extends CommonQueryDTO implements Serializable{

	/**
	 * 产品ID
	 */

	private String ID;
	/**
	 * 产品代码
	 */

	private String ProductCode;

	/**
	 * 产品简称
	 */

	private String ProductAbbName;
	/**
	 * 产品全称
	 */

	private String ProductName;
	/**
	 * OTC_产品大类
	 */
	private String OTC_CPDL;
	/**
	 * OTC_产品子类
	 */
	private String OTC_CPZL;
	/**
	 * 交易状态
	 */
	private String TransactionStatus;
	/**
	 * 发行状态
	 */
	private String IssuingState;

	/**
	 * 风险等级
	 */
	private String RiskLevel;
	/**
	 * 预期收益率
	 */
	private String ExpectedYield;
	/**
	 * 产品净值
	 */
	private String ProductUnitNV;
	/**
	 * 累计净值
	 */
	private String AccumulativeUnitNV;
	/**
	 * 净值日期
	 */
	private String UnitNVDate;

	/**
	 * 个人最低起购认购金额
	 */
	private String GRZD_RGJE;
	/**
	 * 个人最低起购申购金额
	 */
	private String GRZD_SGJE;
	/**
	 * 个人追加最低金额
	 */
	private String GRZJ_ZDJE;
	/**
	 * 个人追加最高金额
	 */
	private String JGZJ_ZGJE;
	/**
	 * 发行开始日期
	 */
	private String IssueStartDate;
	/**
	 * 发行结束日期
	 */
	private String IssueEndDate;

	/**
	 * 允许交易类别
	 */
	private String AllowTradeCategories;

	/**
	 * 产品负责人
	 */

	private String ProductOwner;
	/**
	 * 收费方式
	 */

	private String TollMethod;
	/**
	 * 产品发行人编码
	 */

	private String ProductIssuersEncode;

	/**
	 * 产品发行人名称
	 */
	private String ProductIssuersName;

	/**
	 * 管理人编码
	 */
	private String AdministratorCode;

	/**
	 * 管理人名称
	 */
	private String AdministratorName;

	/**
	 * 托管人代码
	 */
	private String CustodianCode;
	/**
	 * 托管人名称
	 */
	private String CustodianName;

	/**
	 * 预约起始日期
	 */
	private String AppointmentStartTime;

	/**
	 * 预约结束日期
	 */
	private String AppointmentEndTime;
	/**
	 * 登记机构代码
	 */
	private String Registry;

	/**
	 * 类型
	 */
	private String Type;

	/**
	 * 是否展示
	 */
	private String SFClear;
	public OTCProduct() {
		super();
		
	}
	
	public OTCProduct(String productCode, String productAbbName, String productName, String riskLevel,
			String expectedYield, String gRZD_RGJE, String gRZD_SGJE, String gRZJ_ZDJE, String jGZJ_ZGJE,
			String issueStartDate, String issueEndDate, String allowTradeCategories, String productOwner,
			String tollMethod, String productIssuersEncode, String productIssuersName, String custodianName,
			String appointmentStartTime, String appointmentEndTime, String registry, String unitNVDate,
			String issuingState, String administratorName) {
		super();
		ProductCode = productCode;
		ProductAbbName = productAbbName;
		ProductName = productName;
		RiskLevel = riskLevel;
		ExpectedYield = expectedYield;
		GRZD_RGJE = gRZD_RGJE;
		GRZD_SGJE = gRZD_SGJE;
		GRZJ_ZDJE = gRZJ_ZDJE;
		JGZJ_ZGJE = jGZJ_ZGJE;
		IssueStartDate = issueStartDate;
		IssueEndDate = issueEndDate;
		AllowTradeCategories = allowTradeCategories;
		ProductOwner = productOwner;
		TollMethod = tollMethod;
		ProductIssuersEncode = productIssuersEncode;
		ProductIssuersName = productIssuersName;
		CustodianName = custodianName;
		AppointmentStartTime = appointmentStartTime;
		AppointmentEndTime = appointmentEndTime;
		Registry = registry;
		UnitNVDate = unitNVDate;
		IssuingState = issuingState;
		AdministratorName = administratorName;
	}


	
	

	@Override
	public String toString() {
		return "OTCProduct [ProductCode=" + ProductCode + ", IssuingState=" + IssuingState + ", Registry=" + Registry
				+ "]";
	}

	public String getSFClear() {
		return SFClear;
	}
	public void setSFClear(String sFClear) {
		SFClear = sFClear;
	}
	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getJGZJ_ZGJE() {
		return JGZJ_ZGJE;
	}

	public void setJGZJ_ZGJE(String jGZJ_ZGJE) {
		JGZJ_ZGJE = jGZJ_ZGJE;
	}

	public String getAppointmentStartTime() {
		return AppointmentStartTime;
	}

	public void setAppointmentStartTime(String appointmentStartTime) {
		AppointmentStartTime = appointmentStartTime;
	}

	public String getAppointmentEndTime() {
		return AppointmentEndTime;
	}

	public void setAppointmentEndTime(String appointmentEndTime) {
		AppointmentEndTime = appointmentEndTime;
	}

	public String getRegistry() {
		return Registry;
	}

	public void setRegistry(String registry) {
		Registry = registry;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getProductCode() {
		return ProductCode;
	}

	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}

	public String getProductAbbName() {
		return ProductAbbName;
	}

	public void setProductAbbName(String productAbbName) {
		ProductAbbName = productAbbName;
	}

	public String getProductName() {
		return ProductName;
	}

	public void setProductName(String productName) {
		ProductName = productName;
	}

	public String getOTC_CPDL() {
		return OTC_CPDL;
	}

	public void setOTC_CPDL(String oTC_CPDL) {
		OTC_CPDL = oTC_CPDL;
	}

	public String getOTC_CPZL() {
		return OTC_CPZL;
	}

	public void setOTC_CPZL(String oTC_CPZL) {
		OTC_CPZL = oTC_CPZL;
	}

	public String getTransactionStatus() {
		return TransactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		TransactionStatus = transactionStatus;
	}

	public String getIssuingState() {
		return IssuingState;
	}

	public void setIssuingState(String issuingState) {
		IssuingState = issuingState;
	}

	public String getRiskLevel() {
		return RiskLevel;
	}

	public void setRiskLevel(String riskLevel) {
		RiskLevel = riskLevel;
	}

	public String getExpectedYield() {
		return ExpectedYield;
	}

	public void setExpectedYield(String expectedYield) {
		ExpectedYield = expectedYield;
	}

	public String getProductUnitNV() {
		return ProductUnitNV;
	}

	public void setProductUnitNV(String productUnitNV) {
		ProductUnitNV = productUnitNV;
	}

	public String getAccumulativeUnitNV() {
		return AccumulativeUnitNV;
	}

	public void setAccumulativeUnitNV(String accumulativeUnitNV) {
		AccumulativeUnitNV = accumulativeUnitNV;
	}

	public String getUnitNVDate() {
		return UnitNVDate;
	}

	public void setUnitNVDate(String unitNVDate) {
		UnitNVDate = unitNVDate;
	}

	public String getGRZD_RGJE() {
		return GRZD_RGJE;
	}

	public void setGRZD_RGJE(String gRZD_RGJE) {
		GRZD_RGJE = gRZD_RGJE;
	}

	public String getGRZD_SGJE() {
		return GRZD_SGJE;
	}

	public void setGRZD_SGJE(String gRZD_SGJE) {
		GRZD_SGJE = gRZD_SGJE;
	}

	public String getGRZJ_ZDJE() {
		return GRZJ_ZDJE;
	}

	public void setGRZJ_ZDJE(String gRZJ_ZDJE) {
		GRZJ_ZDJE = gRZJ_ZDJE;
	}

	public String getIssueStartDate() {
		return IssueStartDate;
	}

	public void setIssueStartDate(String issueStartDate) {
		IssueStartDate = issueStartDate;
	}

	public String getIssueEndDate() {
		return IssueEndDate;
	}

	public void setIssueEndDate(String issueEndDate) {
		IssueEndDate = issueEndDate;
	}

	public String getAllowTradeCategories() {
		return AllowTradeCategories;
	}

	public void setAllowTradeCategories(String allowTradeCategories) {
		AllowTradeCategories = allowTradeCategories;
	}

	public String getProductOwner() {
		return ProductOwner;
	}

	public void setProductOwner(String productOwner) {
		ProductOwner = productOwner;
	}

	public String getTollMethod() {
		return TollMethod;
	}

	public void setTollMethod(String tollMethod) {
		TollMethod = tollMethod;
	}

	public String getProductIssuersEncode() {
		return ProductIssuersEncode;
	}

	public void setProductIssuersEncode(String productIssuersEncode) {
		ProductIssuersEncode = productIssuersEncode;
	}

	public String getProductIssuersName() {
		return ProductIssuersName;
	}

	public void setProductIssuersName(String productIssuersName) {
		ProductIssuersName = productIssuersName;
	}

	public String getAdministratorCode() {
		return AdministratorCode;
	}

	public void setAdministratorCode(String administratorCode) {
		AdministratorCode = administratorCode;
	}

	public String getAdministratorName() {
		return AdministratorName;
	}

	public void setAdministratorName(String administratorName) {
		AdministratorName = administratorName;
	}

	public String getCustodianCode() {
		return CustodianCode;
	}

	public void setCustodianCode(String custodianCode) {
		CustodianCode = custodianCode;
	}

	public String getCustodianName() {
		return CustodianName;
	}

	public void setCustodianName(String custodianName) {
		CustodianName = custodianName;
	}

	


}
