package com.jk.service;

import java.util.List;

import com.ztzq.product.beans.ProductTask;


public interface ProductTaskService {
	/**
	 * 查询定时任务执行的时间
	 * @return
	 * @throws Exception
	 */
	public List<ProductTask> listTaskInfo() throws Exception;

	/**
	 * 产品basic 采集任务
	 * @throws Exception
	 */
	public void produtBasicTask() throws Exception;
	/**
	 * 非货币采集任务
	 * @throws Exception
	 */
	public void NonCurrentyTaskService() throws Exception;
	/**
	 * 货币采集任务
	 * @throws Exception
	 */
	public void CurrencyTaskService() throws Exception;
	/**
	 * 非货币净值采集任务
	 * @throws Exception
	 */
	public void NonCurrentyNetvalueTaskService() throws Exception;
	/**
	 * 货币万分收益采集任务
	 * @throws Exception
	 */
	public void CurrentyNetvalueTaskService() throws Exception;
	/**
	 * OTC产品采集任务
	 * @throws Exception
	 */
	public void OTCProductTaskService() throws Exception;
	/**
	 * 修改状态值
	 * @throws Exception
	 */
	public void updateStatus(String id) throws Exception;
	/**
	 * 清空数据
	 * @throws Exception
	 */
	public void CleanProductTaskService() throws Exception;
}
