package com.jk.service;

import java.util.List;

import com.ztzq.product.beans.OTCProduct;

public interface OTCProductService {
	/**
	 * 模糊查询产品的状态
	 * @return
	 * @throws Exception
	 */
	public List<OTCProduct> OTCProductCodeOrName(String productCode, int PageNum, int size) throws Exception;
	
	/**
	 * insert一个OTC产品
	 * @return
	 * @throws Exception
	 */
	public boolean insertOTC(OTCProduct otcProduct)throws Exception;
	/**
	 * 查询分页的总数 
	 * @return
	 * @throws Exception
	 */
	public Integer OTCProductCounts(String productCode)throws Exception;

	/**
	 * 根据产品代码进行查询
	 * @return
	 * @throws Exception
	 */
	public OTCProduct findById(String productCode)throws Exception;
	/**
	 * 修改某一个产品状态
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean updateOTCProduct(OTCProduct otcProduct)throws Exception;
	
}
