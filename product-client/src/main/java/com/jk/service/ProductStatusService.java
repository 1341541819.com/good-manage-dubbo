package com.jk.service;

import java.util.List;

import com.ztzq.product.beans.ProductStatus;


public interface ProductStatusService {
	/**
	 * 模糊查询产品的状态
	 * @return
	 * @throws Exception
	 */
	public List<ProductStatus> ProductStatusCodeOrName(String productCode, String productName, int PageNum, int size) throws Exception;
	public List<ProductStatus> listProductStatus(int PageNum, int size)throws Exception;
	/**
	 * 查询分页的总数 
	 * @return
	 * @throws Exception
	 */
	public Integer ProductStatusCounts(String productCode, String productName)throws Exception;

	/**
	 * 根据产品代码进行查询
	 * @return
	 * @throws Exception
	 */
	public String findById(String productCode)throws Exception;
	/**
	 * 修改某一个产品状态
	 * @return
	 * @throws Exception
	 */
	public boolean updateProductStatus(String productCode, String status)throws Exception;
}
