package com.jk.service;

import java.util.List;

import com.ztzq.product.beans.ProductIFClean;

public interface ProductIFCleanService {
	/**
	 * 查询黑白名单代码
	 * @return
	 * @throws Exception
	 */
	public List<ProductIFClean> ProductIFCleanCode(String productCode, int PageNum, int size) throws Exception;
	public List<ProductIFClean> listProductIFClean(int PageNum, int size) throws Exception;
	/**
	 * 查询分页的总数 
	 * @return
	 * @throws Exception
	 */
	public Integer ProductIFCleanCounts(String productCode)throws Exception;
	/**
	 * 根据产品代码进行查询
	 * @return
	 * @throws Exception
	 */
	public ProductIFClean findById(String productCode)throws Exception;
	/**
	 * 修改黑白名单的状态
	 * @return
	 * @throws Exception
	 */
	public boolean updateIFCleanStatus(String productCode, String status)throws Exception;
	/**
	 * insert黑白名单
	 * @return
	 * @throws Exception
	 */
	public boolean insertIFCleanStatus(String productCode, String status)throws Exception;
	/**
	 * 产品子类
	 * @return
	 * @throws Exception
	 */
	public List<ProductIFClean> listOTC_CPZL() throws Exception;
	/**
	 * 修改产品子类的状态
	 * @return
	 * @throws Exception
	 */
	public boolean updateCPZL_Status(String CPZL, String is_sure)throws Exception;
	/**
	 * 获取产品子类的id
	 * @return
	 * @throws Exception
	 */
	public ProductIFClean findBycpzl_Id(String CPZL) throws Exception;
	
	
	/**
	 * insert产品子类
	 * @return
	 * @throws Exception
	 */
	public boolean insertCPZL_Status(String CPZL, String CPZL_Name, String is_sure)throws Exception;
	
	
	/**
	 * 更新产品子类
	 * @param productIFClean
	 * @return
	 */
	public Integer update(ProductIFClean productIFClean);
}
