/*
Navicat MySQL Data Transfer

Source Server         : 139.129.242.133
Source Server Version : 50635
Source Host           : 139.129.242.133:3306
Source Database       : check_in

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2017-05-18 15:05:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for reward
-- ----------------------------
DROP TABLE IF EXISTS `reward`;
CREATE TABLE `reward` (
  `reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_desc` varchar(100) DEFAULT NULL COMMENT '奖励的描述',
  `status` int(2) DEFAULT NULL COMMENT '删除状态,0已删除,1未删除',
  PRIMARY KEY (`reward_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reward
-- ----------------------------
INSERT INTO `reward` VALUES ('1', '积分', '1');
INSERT INTO `reward` VALUES ('2', 'Level2正式奖励', '1');
INSERT INTO `reward` VALUES ('3', 'Level2体验', '1');
INSERT INTO `reward` VALUES ('4', '金币', '1');
INSERT INTO `reward` VALUES ('5', '美女', '1');
INSERT INTO `reward` VALUES ('6', '钻石', '1');
INSERT INTO `reward` VALUES ('7', '土著币', '1');
INSERT INTO `reward` VALUES ('8', '红米4', '1');
INSERT INTO `reward` VALUES ('9', '小米5s', '1');
INSERT INTO `reward` VALUES ('10', '小米笔记本', '1');
INSERT INTO `reward` VALUES ('11', '小米笔记本', '1');
INSERT INTO `reward` VALUES ('12', 'iPad5', '1');

-- ----------------------------
-- Table structure for reward_condition
-- ----------------------------
DROP TABLE IF EXISTS `reward_condition`;
CREATE TABLE `reward_condition` (
  `reward_condition_id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_condition_desc` varchar(30) DEFAULT NULL,
  `status` int(2) DEFAULT NULL COMMENT '删除状态,0已删除,1未删除',
  PRIMARY KEY (`reward_condition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reward_condition
-- ----------------------------
INSERT INTO `reward_condition` VALUES ('1', '等于', '1');
INSERT INTO `reward_condition` VALUES ('2', '大于等于', '1');
INSERT INTO `reward_condition` VALUES ('3', '大于', '1');

-- ----------------------------
-- Table structure for statistic_daily
-- ----------------------------
DROP TABLE IF EXISTS `statistic_daily`;
CREATE TABLE `statistic_daily` (
  `statistic_daily_id` int(11) NOT NULL AUTO_INCREMENT,
  `check_in_date` date DEFAULT NULL COMMENT '签到时间',
  `total_count` int(11) DEFAULT NULL COMMENT '签到总人数',
  `new_count` int(11) DEFAULT NULL COMMENT '新增签到人数',
  `cont_count` int(11) DEFAULT NULL COMMENT '连续签到人数',
  `accu_count` int(11) DEFAULT NULL COMMENT '截止当前时间前一天累计签到人数',
  `reward_count` int(11) DEFAULT NULL COMMENT '总积分数',
  PRIMARY KEY (`statistic_daily_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of statistic_daily
-- ----------------------------
INSERT INTO `statistic_daily` VALUES ('1', '2017-05-01', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('2', '2017-05-02', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('3', '2017-05-03', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('4', '2017-05-04', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('5', '2017-05-05', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('6', '2017-05-06', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('7', '2017-05-07', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('8', '2017-05-08', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('9', '2017-05-09', '3', '2', '3', '9', '80');
INSERT INTO `statistic_daily` VALUES ('10', '2017-05-10', '3', '2', '3', '9', '80');

-- ----------------------------
-- Table structure for statistic_flow
-- ----------------------------
DROP TABLE IF EXISTS `statistic_flow`;
CREATE TABLE `statistic_flow` (
  `statistic_flow_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机号',
  `check_in_time` datetime DEFAULT NULL COMMENT '签到时间',
  `check_in_reward_desc` varchar(255) DEFAULT NULL COMMENT '奖励描述',
  `check_in_flow_id` varchar(255) DEFAULT NULL COMMENT '流水号',
  PRIMARY KEY (`statistic_flow_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of statistic_flow
-- ----------------------------
INSERT INTO `statistic_flow` VALUES ('22', 'f2b5b82d1d7741bbbffe37b214877ece', '18801912359', '2017-04-21 14:12:29', '50 积分', '2017042114477796476367133331');
INSERT INTO `statistic_flow` VALUES ('23', 'f2b5b82d1d7741bbbffe37b214877ece', '18801912359', '2017-04-21 14:12:29', '20 积分', '2017042114477796476367133331');
INSERT INTO `statistic_flow` VALUES ('24', '6b2107ff5d14485db88ca2da0720c59c', '13701769663', '2017-05-02 13:29:11', '10 积分', '2017042116292540813178479439');
INSERT INTO `statistic_flow` VALUES ('25', 'abaae9962d2d4f958e120a36409e4469', '13601877308', '2017-05-02 13:29:14', '40 积分', '2017042115133684499610844906');
INSERT INTO `statistic_flow` VALUES ('26', 'e9937f8ad73a455db2ccf9e837fb4ef7', '13774288473', '2017-05-02 13:29:26', '40 积分', '2017042114502117317506166951');
INSERT INTO `statistic_flow` VALUES ('27', '6b2107ff5d14485db88ca2da0720c59c', '13701769663', '2017-05-02 13:29:11', '10 积分', '2017042116292540813178479439');
INSERT INTO `statistic_flow` VALUES ('28', 'abaae9962d2d4f958e120a36409e4469', '13601877308', '2017-05-02 13:29:14', '40 积分', '2017042115133684499610844906');
INSERT INTO `statistic_flow` VALUES ('29', 'abaae9962d2d4f958e120a36409e4469', '13601877308', '2017-05-02 13:29:14', '40 积分', '2017042115133684499610844906');
INSERT INTO `statistic_flow` VALUES ('30', 'abaae9962d2d4f958e120a36409e4469', '13601877308', '2017-05-02 13:29:14', '40 积分', '2017042115133684499610844906');
INSERT INTO `statistic_flow` VALUES ('31', 'abaae9962d2d4f958e120a36409e4469', '13601877308', '2017-05-02 13:29:14', '40 积分', '2017042115133684499610844906');
INSERT INTO `statistic_flow` VALUES ('32', 'abaae9962d2d4f958e120a36409e4469', '13601877308', '2017-05-02 13:29:14', '40 积分', '2017042115133684499610844906');

-- ----------------------------
-- Table structure for statistic_slot
-- ----------------------------
DROP TABLE IF EXISTS `statistic_slot`;
CREATE TABLE `statistic_slot` (
  `statistic_slot_id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `check_in_count` bigint(20) DEFAULT NULL,
  `new_count` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`statistic_slot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of statistic_slot
-- ----------------------------
INSERT INTO `statistic_slot` VALUES ('1', '2017-05-08 12:00:00', '2017-05-08 13:00:00', '3', '1');
INSERT INTO `statistic_slot` VALUES ('2', '2017-05-07 13:00:00', '2017-05-07 14:00:00', '2', '2');
INSERT INTO `statistic_slot` VALUES ('4', '2017-05-08 13:00:00', '2017-05-08 14:00:00', '2', '2');
INSERT INTO `statistic_slot` VALUES ('7', '2017-05-09 06:00:00', '2017-05-09 07:00:00', '4', '2');
INSERT INTO `statistic_slot` VALUES ('8', '2017-05-09 07:00:00', '2017-05-09 08:00:00', '5', '1');
INSERT INTO `statistic_slot` VALUES ('9', '2017-05-09 08:00:00', '2017-05-09 09:00:00', '6', '3');
INSERT INTO `statistic_slot` VALUES ('10', '2017-05-09 09:00:00', '2017-05-09 10:00:00', '7', '2');
INSERT INTO `statistic_slot` VALUES ('15', '2017-05-09 11:00:00', '2017-05-09 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('16', '2017-05-09 14:00:00', '2017-05-09 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('17', '2017-05-09 15:00:00', '2017-05-09 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('18', '2017-05-09 16:00:00', '2017-05-09 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('19', '2017-05-09 17:00:00', '2017-05-09 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('20', '2017-05-10 12:00:00', '2017-05-10 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('21', '2017-05-10 13:00:00', '2017-05-10 14:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('22', '2017-05-10 14:00:00', '2017-05-10 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('23', '2017-05-10 15:00:00', '2017-05-10 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('24', '2017-05-10 16:00:00', '2017-05-10 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('25', '2017-05-10 16:00:00', '2017-05-10 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('26', '2017-05-10 17:00:00', '2017-05-10 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('27', '2017-05-10 18:00:00', '2017-05-10 19:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('28', '2017-05-10 19:00:00', '2017-05-10 20:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('29', '2017-05-10 20:00:00', '2017-05-10 21:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('30', '2017-05-10 21:00:00', '2017-05-10 22:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('31', '2017-05-10 22:00:00', '2017-05-10 23:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('32', '2017-05-10 23:00:00', '2017-05-11 00:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('33', '2017-05-11 00:00:00', '2017-05-11 01:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('34', '2017-05-11 01:00:00', '2017-05-11 02:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('35', '2017-05-11 02:00:00', '2017-05-11 03:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('36', '2017-05-11 03:00:00', '2017-05-11 04:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('37', '2017-05-11 04:00:00', '2017-05-11 05:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('38', '2017-05-11 05:00:00', '2017-05-11 06:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('39', '2017-05-11 06:00:00', '2017-05-11 07:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('40', '2017-05-11 07:00:00', '2017-05-11 08:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('41', '2017-05-11 08:00:00', '2017-05-11 09:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('42', '2017-05-11 09:00:00', '2017-05-11 10:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('44', '2017-05-11 10:00:00', '2017-05-11 11:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('46', '2017-05-11 11:00:00', '2017-05-11 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('48', '2017-05-11 12:00:00', '2017-05-11 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('50', '2017-05-11 13:00:00', '2017-05-11 14:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('52', '2017-05-11 14:00:00', '2017-05-11 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('54', '2017-05-11 15:00:00', '2017-05-11 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('55', '2017-05-11 17:00:00', '2017-05-11 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('56', '2017-05-11 18:00:00', '2017-05-11 19:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('57', '2017-05-11 19:00:00', '2017-05-11 20:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('58', '2017-05-11 20:00:00', '2017-05-11 21:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('59', '2017-05-11 21:00:00', '2017-05-11 22:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('60', '2017-05-11 22:00:00', '2017-05-11 23:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('61', '2017-05-11 23:00:00', '2017-05-12 00:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('62', '2017-05-12 00:00:00', '2017-05-12 01:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('63', '2017-05-12 01:00:00', '2017-05-12 02:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('64', '2017-05-12 02:00:00', '2017-05-12 03:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('65', '2017-05-12 03:00:00', '2017-05-12 04:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('66', '2017-05-12 04:00:00', '2017-05-12 05:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('67', '2017-05-12 05:00:00', '2017-05-12 06:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('68', '2017-05-12 06:00:00', '2017-05-12 07:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('69', '2017-05-12 07:00:00', '2017-05-12 08:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('70', '2017-05-12 08:00:00', '2017-05-12 09:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('71', '2017-05-12 09:00:00', '2017-05-12 10:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('72', '2017-05-12 10:00:00', '2017-05-12 11:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('73', '2017-05-12 11:00:00', '2017-05-12 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('74', '2017-05-12 12:00:00', '2017-05-12 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('75', '2017-05-12 13:00:00', '2017-05-12 14:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('76', '2017-05-12 14:00:00', '2017-05-12 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('77', '2017-05-12 15:00:00', '2017-05-12 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('78', '2017-05-12 16:00:00', '2017-05-12 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('79', '2017-05-12 17:00:00', '2017-05-12 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('80', '2017-05-12 18:00:00', '2017-05-12 19:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('81', '2017-05-12 19:00:00', '2017-05-12 20:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('82', '2017-05-12 20:00:00', '2017-05-12 21:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('83', '2017-05-12 21:00:00', '2017-05-12 22:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('84', '2017-05-12 22:00:00', '2017-05-12 23:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('85', '2017-05-12 23:00:00', '2017-05-13 00:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('86', '2017-05-13 00:00:00', '2017-05-13 01:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('87', '2017-05-13 01:00:00', '2017-05-13 02:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('88', '2017-05-13 02:00:00', '2017-05-13 03:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('89', '2017-05-13 03:00:00', '2017-05-13 04:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('90', '2017-05-13 04:00:00', '2017-05-13 05:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('91', '2017-05-13 05:00:00', '2017-05-13 06:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('92', '2017-05-13 06:00:00', '2017-05-13 07:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('93', '2017-05-13 07:00:00', '2017-05-13 08:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('94', '2017-05-13 08:00:00', '2017-05-13 09:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('95', '2017-05-13 11:00:00', '2017-05-13 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('96', '2017-05-13 12:00:00', '2017-05-13 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('97', '2017-05-13 13:00:00', '2017-05-13 14:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('98', '2017-05-13 14:00:00', '2017-05-13 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('99', '2017-05-13 15:00:00', '2017-05-13 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('100', '2017-05-13 16:00:00', '2017-05-13 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('101', '2017-05-13 17:00:00', '2017-05-13 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('102', '2017-05-13 18:00:00', '2017-05-13 19:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('103', '2017-05-13 19:00:00', '2017-05-13 20:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('104', '2017-05-13 20:00:00', '2017-05-13 21:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('105', '2017-05-13 21:00:00', '2017-05-13 22:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('106', '2017-05-13 22:00:00', '2017-05-13 23:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('107', '2017-05-13 23:00:00', '2017-05-14 00:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('108', '2017-05-14 00:00:00', '2017-05-14 01:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('109', '2017-05-14 01:00:00', '2017-05-14 02:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('110', '2017-05-14 02:00:00', '2017-05-14 03:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('111', '2017-05-14 03:00:00', '2017-05-14 04:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('112', '2017-05-14 04:00:00', '2017-05-14 05:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('113', '2017-05-14 05:00:00', '2017-05-14 06:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('114', '2017-05-14 06:00:00', '2017-05-14 07:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('115', '2017-05-14 07:00:00', '2017-05-14 08:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('116', '2017-05-14 08:00:00', '2017-05-14 09:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('117', '2017-05-14 09:00:00', '2017-05-14 10:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('118', '2017-05-14 10:00:00', '2017-05-14 11:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('119', '2017-05-14 11:00:00', '2017-05-14 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('120', '2017-05-14 12:00:00', '2017-05-14 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('121', '2017-05-14 13:00:00', '2017-05-14 14:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('122', '2017-05-14 14:00:00', '2017-05-14 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('123', '2017-05-14 15:00:00', '2017-05-14 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('124', '2017-05-14 16:00:00', '2017-05-14 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('125', '2017-05-14 17:00:00', '2017-05-14 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('126', '2017-05-14 18:00:00', '2017-05-14 19:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('127', '2017-05-14 19:00:00', '2017-05-14 20:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('128', '2017-05-14 20:00:00', '2017-05-14 21:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('129', '2017-05-14 21:00:00', '2017-05-14 22:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('130', '2017-05-14 22:00:00', '2017-05-14 23:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('131', '2017-05-14 23:00:00', '2017-05-15 00:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('132', '2017-05-15 00:00:00', '2017-05-15 01:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('133', '2017-05-15 01:00:00', '2017-05-15 02:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('134', '2017-05-15 02:00:00', '2017-05-15 03:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('135', '2017-05-15 03:00:00', '2017-05-15 04:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('136', '2017-05-15 04:00:00', '2017-05-15 05:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('137', '2017-05-15 05:00:00', '2017-05-15 06:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('138', '2017-05-15 06:00:00', '2017-05-15 07:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('139', '2017-05-15 07:00:00', '2017-05-15 08:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('140', '2017-05-15 08:00:00', '2017-05-15 09:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('141', '2017-05-15 09:00:00', '2017-05-15 10:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('142', '2017-05-15 10:00:00', '2017-05-15 11:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('143', '2017-05-15 11:00:00', '2017-05-15 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('144', '2017-05-15 12:00:00', '2017-05-15 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('145', '2017-05-15 13:00:00', '2017-05-15 14:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('146', '2017-05-15 14:00:00', '2017-05-15 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('147', '2017-05-15 15:00:00', '2017-05-15 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('148', '2017-05-15 16:00:00', '2017-05-15 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('149', '2017-05-15 17:00:00', '2017-05-15 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('150', '2017-05-15 18:00:00', '2017-05-15 19:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('151', '2017-05-15 19:00:00', '2017-05-15 20:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('152', '2017-05-15 20:00:00', '2017-05-15 21:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('153', '2017-05-15 21:00:00', '2017-05-15 22:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('154', '2017-05-15 22:00:00', '2017-05-15 23:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('155', '2017-05-15 23:00:00', '2017-05-16 00:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('156', '2017-05-16 00:00:00', '2017-05-16 01:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('157', '2017-05-16 01:00:00', '2017-05-16 02:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('158', '2017-05-16 02:00:00', '2017-05-16 03:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('159', '2017-05-16 03:00:00', '2017-05-16 04:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('160', '2017-05-16 04:00:00', '2017-05-16 05:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('161', '2017-05-16 05:00:00', '2017-05-16 06:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('162', '2017-05-16 06:00:00', '2017-05-16 07:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('163', '2017-05-16 07:00:00', '2017-05-16 08:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('164', '2017-05-16 08:00:00', '2017-05-16 09:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('165', '2017-05-16 09:00:00', '2017-05-16 10:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('166', '2017-05-16 10:00:00', '2017-05-16 11:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('167', '2017-05-16 11:00:00', '2017-05-16 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('168', '2017-05-16 12:00:00', '2017-05-16 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('169', '2017-05-16 13:00:00', '2017-05-16 14:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('170', '2017-05-16 14:00:00', '2017-05-16 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('171', '2017-05-16 15:00:00', '2017-05-16 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('172', '2017-05-16 16:00:00', '2017-05-16 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('173', '2017-05-16 17:00:00', '2017-05-16 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('174', '2017-05-16 18:00:00', '2017-05-16 19:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('175', '2017-05-16 19:00:00', '2017-05-16 20:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('176', '2017-05-16 20:00:00', '2017-05-16 21:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('177', '2017-05-16 21:00:00', '2017-05-16 22:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('178', '2017-05-16 22:00:00', '2017-05-16 23:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('179', '2017-05-16 23:00:00', '2017-05-17 00:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('180', '2017-05-17 00:00:00', '2017-05-17 01:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('181', '2017-05-17 01:00:00', '2017-05-17 02:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('182', '2017-05-17 02:00:00', '2017-05-17 03:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('183', '2017-05-17 03:00:00', '2017-05-17 04:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('184', '2017-05-17 04:00:00', '2017-05-17 05:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('185', '2017-05-17 05:00:00', '2017-05-17 06:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('186', '2017-05-17 06:00:00', '2017-05-17 07:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('187', '2017-05-17 07:00:00', '2017-05-17 08:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('188', '2017-05-17 08:00:00', '2017-05-17 09:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('189', '2017-05-17 09:00:00', '2017-05-17 10:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('190', '2017-05-17 10:00:00', '2017-05-17 11:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('191', '2017-05-17 11:00:00', '2017-05-17 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('192', '2017-05-17 12:00:00', '2017-05-17 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('193', '2017-05-17 13:00:00', '2017-05-17 14:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('194', '2017-05-17 14:00:00', '2017-05-17 15:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('195', '2017-05-17 15:00:00', '2017-05-17 16:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('196', '2017-05-17 16:00:00', '2017-05-17 17:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('197', '2017-05-17 17:00:00', '2017-05-17 18:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('198', '2017-05-17 18:00:00', '2017-05-17 19:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('199', '2017-05-17 19:00:00', '2017-05-17 20:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('200', '2017-05-17 20:00:00', '2017-05-17 21:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('201', '2017-05-17 21:00:00', '2017-05-17 22:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('202', '2017-05-17 22:00:00', '2017-05-17 23:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('203', '2017-05-17 23:00:00', '2017-05-18 00:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('204', '2017-05-18 00:00:00', '2017-05-18 01:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('205', '2017-05-18 01:00:00', '2017-05-18 02:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('206', '2017-05-18 02:00:00', '2017-05-18 03:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('207', '2017-05-18 03:00:00', '2017-05-18 04:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('208', '2017-05-18 04:00:00', '2017-05-18 05:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('209', '2017-05-18 05:00:00', '2017-05-18 06:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('210', '2017-05-18 06:00:00', '2017-05-18 07:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('211', '2017-05-18 07:00:00', '2017-05-18 08:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('212', '2017-05-18 08:00:00', '2017-05-18 09:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('213', '2017-05-18 09:00:00', '2017-05-18 10:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('214', '2017-05-18 10:00:00', '2017-05-18 11:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('215', '2017-05-18 11:00:00', '2017-05-18 12:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('216', '2017-05-18 12:00:00', '2017-05-18 13:00:00', '0', '0');
INSERT INTO `statistic_slot` VALUES ('217', '2017-05-18 13:00:00', '2017-05-18 14:00:00', '1', '1');
INSERT INTO `statistic_slot` VALUES ('218', '2017-05-18 14:00:00', '2017-05-18 15:00:00', '0', '0');

-- ----------------------------
-- Table structure for user_check_in
-- ----------------------------
DROP TABLE IF EXISTS `user_check_in`;
CREATE TABLE `user_check_in` (
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  `accu_check_in` int(11) NOT NULL DEFAULT '0' COMMENT '累计签到次数',
  `cont_check_in` int(11) NOT NULL DEFAULT '0' COMMENT '连续签到次数',
  `last_check_in` date DEFAULT NULL COMMENT '最后一次签到的时间',
  `if_integral_user` int(11) DEFAULT NULL,
  UNIQUE KEY `user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_check_in
-- ----------------------------
INSERT INTO `user_check_in` VALUES ('1c5795c95b5649a9a6724608e59516cd', '1', '1', '2017-04-20', '1');
INSERT INTO `user_check_in` VALUES ('6b2107ff5d14485db88ca2da0720c59c', '1', '1', '2017-04-20', '1');
INSERT INTO `user_check_in` VALUES ('6e0b48f279e84d81aa5adb709c2e9744', '1', '1', '2017-04-19', null);
INSERT INTO `user_check_in` VALUES ('abaae9962d2d4f958e120a36409e4469', '1', '1', '2017-04-20', '1');
INSERT INTO `user_check_in` VALUES ('b1015b8fe4d64cb69428587b41e85e5a', '1', '1', '2017-04-20', '1');
INSERT INTO `user_check_in` VALUES ('e9937f8ad73a455db2ccf9e837fb4ef7', '1', '1', '2017-04-20', '1');
INSERT INTO `user_check_in` VALUES ('f2b5b82d1d7741bbbffe37b214877ece', '1', '1', '2017-04-20', '1');

-- ----------------------------
-- Table structure for user_check_in_calander
-- ----------------------------
DROP TABLE IF EXISTS `user_check_in_calander`;
CREATE TABLE `user_check_in_calander` (
  `mobile` varchar(16) DEFAULT NULL,
  `check_in_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `check_in_user_id` varchar(32) DEFAULT NULL COMMENT '签到的用户Id\r\n',
  `check_in_date` date DEFAULT NULL COMMENT '格式为YYYY-MM-DD的签到日期',
  `is_first_check_in` tinyint(1) NOT NULL,
  `is_continue_check_in` tinyint(1) NOT NULL,
  PRIMARY KEY (`check_in_id`),
  UNIQUE KEY `check_in_id` (`check_in_id`) USING BTREE,
  UNIQUE KEY `c_u_unique` (`check_in_user_id`,`check_in_date`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_check_in_calander
-- ----------------------------
INSERT INTO `user_check_in_calander` VALUES ('13701769663', '99', '6b2107ff5d14485db88ca2da0720c59c', '2017-05-04', '1', '1');
INSERT INTO `user_check_in_calander` VALUES ('13601877308', '100', 'abaae9962d2d4f958e120a36409e4469', '2017-05-04', '1', '1');
INSERT INTO `user_check_in_calander` VALUES ('13774288473', '101', 'e9937f8ad73a455db2ccf9e837fb4ef7', '2017-05-03', '1', '1');
INSERT INTO `user_check_in_calander` VALUES ('18565582332', '102', '6e0b48f279e84d81aa5adb709c2e9744', '2017-04-18', '1', '1');
INSERT INTO `user_check_in_calander` VALUES ('15021435424', '103', 'b1015b8fe4d64cb69428587b41e85e5a', '2017-04-04', '1', '0');
INSERT INTO `user_check_in_calander` VALUES ('18801912359', '104', 'f2b5b82d1d7741bbbffe37b214877ece', '2017-05-02', '1', '1');
INSERT INTO `user_check_in_calander` VALUES ('13816222549', '105', '1c5795c95b5649a9a6724608e59516cd', '2017-04-18', '1', '1');
INSERT INTO `user_check_in_calander` VALUES ('13816222549', '119', '1c5795c95b5649a9a6724608e59516cd', '2017-04-20', '0', '1');
INSERT INTO `user_check_in_calander` VALUES ('15021435424', '120', 'b1015b8fe4d64cb69428587b41e85e5a', '2017-05-05', '1', '1');
INSERT INTO `user_check_in_calander` VALUES ('13774288473', '121', 'e9937f8ad73a455db2ccf9e837fb4ef7', '2017-05-04', '0', '1');

-- ----------------------------
-- Table structure for user_check_in_calander_reward
-- ----------------------------
DROP TABLE IF EXISTS `user_check_in_calander_reward`;
CREATE TABLE `user_check_in_calander_reward` (
  `reward_param` varchar(10) DEFAULT NULL,
  `check_in_id` bigint(20) DEFAULT NULL,
  `reward_unit` varchar(10) DEFAULT NULL,
  `flow_id` varchar(60) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  KEY `checn_in_id_index` (`check_in_id`) USING BTREE,
  KEY `reward_flow_f_k` (`flow_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_check_in_calander_reward
-- ----------------------------
INSERT INTO `user_check_in_calander_reward` VALUES ('50', '104', '积分', '2017042114477796476367133331', '2017-05-08 16:12:29');
INSERT INTO `user_check_in_calander_reward` VALUES ('20', '104', '积分', '2017042114477796476367133331', '2017-05-04 14:12:29');
INSERT INTO `user_check_in_calander_reward` VALUES ('10', '99', '积分', '2017042116292540813178479439', '2017-05-08 16:29:11');
INSERT INTO `user_check_in_calander_reward` VALUES ('40', '100', '积分', '2017042115133684499610844906', '2017-05-02 13:29:14');
INSERT INTO `user_check_in_calander_reward` VALUES ('40', '101', '积分', '2017042114502117317506166951', '2017-05-09 10:29:26');
INSERT INTO `user_check_in_calander_reward` VALUES ('50', '102', '积分', '2017042114526392019620127857', '2017-04-18 13:29:41');
INSERT INTO `user_check_in_calander_reward` VALUES ('50', '103', '积分', '2017042114477796476367133331', '2017-05-18 13:29:52');

-- ----------------------------
-- Table structure for user_check_in_flow
-- ----------------------------
DROP TABLE IF EXISTS `user_check_in_flow`;
CREATE TABLE `user_check_in_flow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flow_id` varchar(60) DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `is_used` bit(1) DEFAULT b'0',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `flow_id_index` (`flow_id`) USING BTREE,
  KEY `user_f_k` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_check_in_flow
-- ----------------------------
INSERT INTO `user_check_in_flow` VALUES ('62', '2017042116292540813178479439', 'f2b5b82d1d7741bbbffe37b214877ece', '', '2017-04-21 16:36:54');
INSERT INTO `user_check_in_flow` VALUES ('61', '2017042115133684499610844906', 'f2b5b82d1d7741bbbffe37b214877ece', '', '2017-04-21 15:01:56');
INSERT INTO `user_check_in_flow` VALUES ('60', '2017042114502117317506166951', 'abaae9962d2d4f958e120a36409e4469', '', '2017-04-21 14:33:32');
INSERT INTO `user_check_in_flow` VALUES ('59', '2017042114526392019620127857', '1c5795c95b5649a9a6724608e59516cd', '', '2017-04-21 14:12:35');
INSERT INTO `user_check_in_flow` VALUES ('58', '2017042114477796476367133331', 'e9937f8ad73a455db2ccf9e837fb4ef7', '', '2017-04-21 14:12:29');

-- ----------------------------
-- Table structure for user_check_in_rules
-- ----------------------------
DROP TABLE IF EXISTS `user_check_in_rules`;
CREATE TABLE `user_check_in_rules` (
  `check_in_rule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '签到规则id',
  `reward_condition` int(11) DEFAULT NULL COMMENT '奖励条件',
  `check_in_type` int(11) DEFAULT NULL COMMENT '签到类型',
  `check_in_days` int(11) DEFAULT NULL COMMENT '签到天数,与签到特殊日期两者只能有一个有值',
  `check_in_date` date DEFAULT NULL COMMENT '特殊签到日期',
  `reward_id` int(11) DEFAULT NULL COMMENT '奖励的内容，对应到reward表主键',
  `reward_amount` int(11) DEFAULT NULL COMMENT '奖励的数量',
  `reward_remark` varchar(200) DEFAULT NULL COMMENT '对奖励规则的描述',
  `reward_status` tinyint(4) DEFAULT '1' COMMENT '是否启用',
  `reward_priori` smallint(6) DEFAULT NULL COMMENT '奖励规则的优先级，低优先级的先被触发',
  `reward_unit` varchar(10) DEFAULT NULL COMMENT '奖励的单位，天、积分、月等',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`check_in_rule_id`),
  KEY `reward_f_k` (`reward_id`),
  KEY `condition_f_k` (`reward_condition`),
  KEY `type_f_k` (`check_in_type`),
  CONSTRAINT `condition_f_k` FOREIGN KEY (`reward_condition`) REFERENCES `reward_condition` (`reward_condition_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reward_f_k` FOREIGN KEY (`reward_id`) REFERENCES `reward` (`reward_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `type_f_k` FOREIGN KEY (`check_in_type`) REFERENCES `user_check_in_type` (`check_in_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_check_in_rules
-- ----------------------------
INSERT INTO `user_check_in_rules` VALUES ('1', '1', '2', '1', null, '1', '10', '连续签到1天10积分', '1', '1', '分', '2017-04-20 14:35:28');
INSERT INTO `user_check_in_rules` VALUES ('2', '1', '2', '2', null, '1', '20', '连续签到2天20积分', '1', '1', '分', '2017-04-19 10:15:11');
INSERT INTO `user_check_in_rules` VALUES ('3', '1', '2', '3', null, '1', '30', '连续签到3天30积分', '1', '1', '分', '2017-04-19 10:15:13');
INSERT INTO `user_check_in_rules` VALUES ('4', '1', '2', '4', null, '1', '40', '连续签到4天40积分', '1', '1', '分', '2017-04-19 10:15:15');
INSERT INTO `user_check_in_rules` VALUES ('5', '2', '2', '5', null, '1', '50', '连续签到5天以上50积分', '1', '1', '分', '2017-04-19 10:15:19');
INSERT INTO `user_check_in_rules` VALUES ('6', '1', '1', null, '2017-05-01', '2', '1', '5.1签到送一个月level2', '1', '1', '月', '2017-04-19 16:14:50');
INSERT INTO `user_check_in_rules` VALUES ('13', '1', '2', '2', null, '4', '20', '是多喝点水', '0', '1', ' 枚', '2017-04-24 12:18:59');
INSERT INTO `user_check_in_rules` VALUES ('14', '1', '1', null, '2017-06-01', '4', '40', '在查杀===', '0', '1', '枚', '2017-04-24 11:13:11');
INSERT INTO `user_check_in_rules` VALUES ('15', '1', '1', null, '2017-06-02', '5', '1', '山海经', '0', '1', '个', '2017-04-24 11:16:16');
INSERT INTO `user_check_in_rules` VALUES ('16', '1', '1', null, '2017-07-01', '1', '30', '阿斯达所', '0', '1', '个', '2017-04-24 12:10:16');

-- ----------------------------
-- Table structure for user_check_in_type
-- ----------------------------
DROP TABLE IF EXISTS `user_check_in_type`;
CREATE TABLE `user_check_in_type` (
  `check_in_type_id` int(6) NOT NULL AUTO_INCREMENT COMMENT '奖励类型（枚举）',
  `check_in_desc` varchar(100) DEFAULT NULL,
  `status` int(2) DEFAULT NULL COMMENT '删除状态,1未删除,0已删除',
  PRIMARY KEY (`check_in_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_check_in_type
-- ----------------------------
INSERT INTO `user_check_in_type` VALUES ('1', '单日签到', '1');
INSERT INTO `user_check_in_type` VALUES ('2', '连续签到', '1');
INSERT INTO `user_check_in_type` VALUES ('3', '????', '1');
INSERT INTO `user_check_in_type` VALUES ('4', '隔日签到', '0');

-- ----------------------------
-- Procedure structure for CONT_CHEINCK
-- ----------------------------
DROP PROCEDURE IF EXISTS `CONT_CHEINCK`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `CONT_CHEINCK`(IN userId varchar(50),IN date varchar(20),IN reward int(10),IN flowId varchar(50),IN mobile varchar(20))
begin
DECLARE t_error INTEGER DEFAULT 0;  
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET t_error=1;  
       START TRANSACTION;  
update user_check_in set cont_check_in=cont_check_in+1,accu_check_in=accu_check_in+1,last_check_in=date where user_id=userId;
insert into user_check_in_calander (check_in_user_id,check_in_date,mobile) VALUE (userId,date,mobile);
insert into user_check_in_flow (flow_id,user_id) VALUE (flowId,userId);
select check_in_id INTO @check_in_id  from user_check_in_calander where check_in_user_id =userId and check_in_date=date;
insert into user_check_in_calander_reward (check_in_id,reward_param,reward_unit,flow_id) VALUE (@check_in_id,reward,'积分',flowId);
IF t_error = 1 THEN  
            ROLLBACK;  
        ELSE  
            COMMIT;  
        END IF;  
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for IF_INTEGRAL_USER
-- ----------------------------
DROP PROCEDURE IF EXISTS `IF_INTEGRAL_USER`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `IF_INTEGRAL_USER`(IN userId varchar(50))
begin
update  user_check_in set if_integral_user=1 where user_id=userId;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for INT_CHEINCK
-- ----------------------------
DROP PROCEDURE IF EXISTS `INT_CHEINCK`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `INT_CHEINCK`(IN userId varchar(50),IN accu int(10),IN cont int(10),IN date varchar(20),IN reward int(10),IN flowId varchar(50),IN mobile varchar(20))
begin
 DECLARE t_error INTEGER DEFAULT 0;  
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET t_error=1;  
       START TRANSACTION;  
replace into user_check_in (user_id,accu_check_in,cont_check_in,last_check_in) VALUE (userId,accu,cont,date);
insert into user_check_in_calander (check_in_user_id,check_in_date,mobile) VALUE (userId,date,mobile);
insert into user_check_in_flow (flow_id,user_id) VALUE (flowId,userId);
select check_in_id INTO @check_in_id  from user_check_in_calander where check_in_user_id =userId and check_in_date=date;
insert into user_check_in_calander_reward (check_in_id,reward_param,reward_unit,flow_id) VALUE (@check_in_id,reward,'积分',flowId);
 IF t_error = 1 THEN  
            ROLLBACK;  
        ELSE  
            COMMIT;  
        END IF;  
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for NON_CONT_CHEINCK
-- ----------------------------
DROP PROCEDURE IF EXISTS `NON_CONT_CHEINCK`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `NON_CONT_CHEINCK`(IN userId varchar(50))
begin
update  user_check_in set cont_check_in=0,accu_check_in=accu_check_in+1 where user_id=userId;
end
;;
DELIMITER ;
