package com.jk.controller.check;

import com.github.pagehelper.PageInfo;
import com.jk.annotation.OperationLog;
import com.jk.controller.BaseController;
import com.jk.model.Role;
import com.ztzq.check.beans.Reward;
import com.ztzq.check.service.RewardService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author cuiP
 * Created by JK on 2017/1/19.
 */
@Controller
@RequestMapping("/reward")
public class RewardController extends BaseController {

    private static final String BASE_PATH = "admin/check/";

    @Autowired
    private RewardService rewardService;


    /**
     * 分页查询管理员列表
     *
     * @param pageNum   当前页码
     * @param rewardName  奖励名称
     * @param modelMap
     * @return
     */
    //@RequiresPermissions("reward:list")
    @GetMapping(value = "/list")
    public String list(
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
            String rewardName, ModelMap modelMap) throws Exception {
        log.debug("分页查询奖励列表参数! pageNum = {}, rewardName = {}", pageNum, rewardName);
        Reward reward = new Reward();
        reward.setKeyword1(rewardName);
        //封装分页信息
        if (pageNum != null){
            reward.setCurrentPage(pageNum);
        }
        PageInfo<Reward> listRewardPage = rewardService.getListRewardPage(reward);
        log.info("分页查询奖励列表结果！ pageInfo = {}",listRewardPage);
        modelMap.put("pageInfo", listRewardPage);
        modelMap.put("rewardName", rewardName);
        return BASE_PATH + "reward-list";
    }

    /**
     * 跳转到奖励添加页面
     * @return
     */
    @OperationLog(value = "添加奖励")
    //@RequiresPermissions("reward:create")
    @GetMapping(value = "/to/add")
    public String add(){
        try {
            log.info("跳转到奖励添加页面!");
            return BASE_PATH + "reward-add";

        }catch (Exception e){
            log.error("跳转失败! e = {}", e);
            e.printStackTrace();
            return "404";
        }
    }

    /**
     * 奖励\禁用|启用
     * @param id 操作奖励数据对应的id
     * @return
     */
    @OperationLog(value = "禁用|启用奖励")
    //@RequiresPermissions("reward:status")
    @RequestMapping(value = "/status/{id}")
    public ResponseEntity<String> updateStatus(@PathVariable("id") Long id,String status){
        try {
            log.debug("禁用|启用奖励参数! id = {},status = {}", id,status);
            //禁用启用
            Integer integer = rewardService.updateStatus(id.toString(), Integer.parseInt(status));
            if (integer > 0){
                log.info("禁用|启用奖励成功! id = {}", id);
                return ResponseEntity.status(HttpStatus.OK).build();
            }else {
                log.info("禁用|启用奖励失败! id = {}", id);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } catch (Exception e) {
            log.error("禁用|启用奖励失败! id = {}, e = {}", id, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
