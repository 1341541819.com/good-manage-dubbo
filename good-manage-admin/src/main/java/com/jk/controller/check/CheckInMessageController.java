package com.jk.controller.check;

import com.github.pagehelper.PageInfo;
import com.jk.controller.BaseController;
import com.ztzq.check.beans.CheckInMessage;
import com.ztzq.check.beans.Reward;
import com.ztzq.check.service.CheckInMessageService;
import com.ztzq.check.service.RewardService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author cuiP
 * Created by JK on 2017/1/19.
 */
@Controller
@RequestMapping("/checkinmessage")
public class CheckInMessageController extends BaseController {

    private static final String BASE_PATH = "admin/check/";

    @Autowired
    CheckInMessageService checkInMessageService;

    /**
     * 分页查询奖励流水信息
     *
     * @param pageNum  当前页码
     * @param mobile  手机号
     * @param userid  用户id
     * @param flowid  流水号
     * @param startdate 开始时间
     * @param enddate   结束时间
     * @return
     */
    //@RequiresPermissions("reward:list")
    @GetMapping(value = "/list")
    public String list(
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
            String mobile,String userid,String flowid,String startdate,String enddate, ModelMap modelMap) throws Exception {
        log.debug("分页查询奖励流水信息列表参数! pageNum = {}, mobile = {}, userid = {}, flowid = {}, startdate = {}, enddate = {}", pageNum, mobile,userid,flowid,startdate,enddate);
        CheckInMessage checkInMessage = new CheckInMessage();
        //封装分页数据
        if(pageNum != null){
            checkInMessage.setCurrentPage(pageNum);
        }
        //封装查询条件
        if(StringUtils.isNotBlank(mobile)){
            checkInMessage.setKeyword1(mobile);
        }
        if(StringUtils.isNotBlank(userid)){
            checkInMessage.setKeyword2(userid);
        }
        if(StringUtils.isNotBlank(flowid)){
            checkInMessage.setKeyword3(flowid);
        }
        if(StringUtils.isNotBlank(startdate)){
            checkInMessage.setKeyword4(startdate);
        }
        if(StringUtils.isNotBlank(enddate)){
            checkInMessage.setKeyword5(enddate);
        }
        //进行分页数据查询
        PageInfo<CheckInMessage> pageInfo = checkInMessageService.getListCheckInMessagePage(checkInMessage);
//        for (CheckInMessage reward1: pageInfo.getList()) {
//            System.out.println(reward1.getAccuCheckIn());
//            System.out.println(reward1.getContCheckIn());
//        }
        log.info("分页查询奖励列表结果！ pageInfo = {}",pageInfo);
        modelMap.put("pageInfo", pageInfo);
        modelMap.put("mobile", mobile);
        modelMap.put("userid", userid);
        modelMap.put("flowid", flowid);
        modelMap.put("startdate", startdate);
        modelMap.put("enddate", enddate);
        return BASE_PATH + "checkinmessage-list";
    }
}
