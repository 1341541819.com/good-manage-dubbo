$(function () {

});

/*
 参数解释：
 title	标题
 url		请求的url
 id		需要操作的数据id
 w		弹出层宽度（缺省调默认值）
 h		弹出层高度（缺省调默认值）
 */
/*奖励-增加*/
function checkinmessage_add(title,url,w,h){
    layer_show(title,url,w,h);
}
/*奖励-删除*/
function checkinmessage_del(status, url){
    if (status == 0){
        layer.confirm('确认要删除吗？',function(index){
            //此处请求后台程序，下方是成功后的前台处理……
            $.ajax({
                type:"PUT",
                dataType:"json",
                url: url,
                data:{
                    'status' : status
                },
                statusCode: {
                    200 : function(data){
                        succeedMessage("删除状态成功");
                        setTimeout(window.location.reload(),2000);
                    },
                    404 : function(data){
                        errorMessage("删除状态失败");
                    },
                    500 : function(){
                        errorMessage('系统错误!');
                    }
                }
            });
        });
    }else {
        layer.confirm('确认要还原吗？',function(index){
            //此处请求后台程序，下方是成功后的前台处理……
            $.ajax({
                type:"PUT",
                dataType:"json",
                url: url,
                data:{
                    'status' : status
                },
                statusCode: {
                    200 : function(data){
                        succeedMessage("还原状态成功");
                        setTimeout(window.location.reload(),2000);
                    },
                    404 : function(data){
                        errorMessage("还原状态失败");
                    },
                    500 : function(){
                        errorMessage('系统错误!');
                    }
                }
            });
        });
    }
}
/*奖励-编辑*/
function checkinmessage_edi(title,url,w,h){
    layer_show(title,url,w,h);
}
